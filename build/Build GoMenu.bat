@echo off
rem Win32Go GoMenus Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoMenu..............
go install gitlab.com\win32go\utopia\code\demos\GoMenu
pause

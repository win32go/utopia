@echo off
rem Win32Go GoApplication Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoApplication..............
go install gitlab.com\win32go\utopia\code\demos\GoApplication
pause

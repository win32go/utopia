@echo off
rem Win32Go GoTextEdit Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoTextEdit..............
go install gitlab.com\win32go\utopia\code\demos\GoTextEdit
pause

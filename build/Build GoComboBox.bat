@echo off
rem Win32Go GoComboBox Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoComboBox..............
go install gitlab.com\win32go\utopia\code\demos\GoComboBox
pause

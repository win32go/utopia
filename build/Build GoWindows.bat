@echo off
rem Win32Go GoWindows Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoWindows..............
go install gitlab.com\win32go\utopia\code\demos\GoWindows
pause

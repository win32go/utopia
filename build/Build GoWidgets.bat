@echo off
rem Win32Go GoWidgets Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoWidgets..............
go install gitlab.com\win32go\utopia\code\demos\GoWidgets
pause

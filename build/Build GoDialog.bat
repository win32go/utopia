@echo off
rem Win32Go GoDialog Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoDialog..............
go install gitlab.com\win32go\utopia\code\demos\GoDialog
pause

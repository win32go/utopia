@echo off
rem Win32Go GoColorTest Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoColorTest..............
go install gitlab.com\win32go\utopia\code\demos\GoColorTest
pause

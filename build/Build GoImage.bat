@echo off
rem Win32Go GoImage Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoImage..............
go install gitlab.com\win32go\utopia\code\demos\GoImage
pause

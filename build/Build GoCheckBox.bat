@echo off
rem Win32Go GoCheckBox Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoCheckBox..............
go install gitlab.com\win32go\utopia\code\demos\GoCheckBox
pause

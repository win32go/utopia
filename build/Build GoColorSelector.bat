@echo off
rem Win32Go GoColorTest Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\bin
echo Installing GoColorSelector..............
go install gitlab.com\win32go\utopia\code\helpers\GoColorSelector
pause

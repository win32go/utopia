@echo off
rem Win32Go GoMessageBox Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoMessageBox..............
go install gitlab.com\win32go\utopia\code\demos\GoMessageBox
pause

@echo off
rem Win32Go GoProgressBar Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoProgressBar..............
go install gitlab.com\win32go\utopia\code\demos\GoProgressBar
pause

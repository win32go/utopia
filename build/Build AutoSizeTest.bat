@echo off
rem Win32Go AutoSizeTest Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing AutoSizeTest..............
go install gitlab.com\win32go\utopia\code\demos\AutoSizeTest
pause

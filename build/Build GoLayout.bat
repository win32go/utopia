@echo off
rem Win32Go GoLayout Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoLayout..............
go install gitlab.com\win32go\utopia\code\demos\GoLayout
pause

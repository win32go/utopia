@echo off
rem Win32Go GoLineEdit Demo Install
set GO111MODULE=off
set GOBIN=C:\godev\src\gitlab.com\win32go\utopia\demos
echo Installing GoLineEdit..............
go install gitlab.com\win32go\utopia\code\demos\GoLineEdit
pause

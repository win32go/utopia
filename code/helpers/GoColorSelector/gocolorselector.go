/* win32go/code/demos/GoColorSelector/gocolorselector.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var color[24] *ui.GoLabelObj

var colorDict = func() map[int]string { return map[int]string {
		0	:	"Transparent",
		1	:	"Color_AliceBlue",
		2	:	"Color_AntiqueWhite",
		3	:	"Color_Aqua",
		4	:	"Color_Aquamarine",
		5	:	"Color_Azure",
		6	:	"Color_Beige",
		7	:	"Color_Bisque",
		8	:	"Color_Black",
		9	:	"Color_BlanchedAlmond",
		10	:	"Color_Blue",
		11	:	"Color_BlueViolet",
		12	:	"Color_Brown",
		13	:	"Color_BurlyWood",
		14	:	"Color_CadetBlue",
		15	:	"Color_Chartreuse",
		16	:	"Color_Chocolate",
		17	:	"Color_Coral",
		18	:	"Color_CornflowerBlue",
		19	:	"Color_Cornsilk",
		20	:	"Color_Crimson",
		21	:	"Color_Cyan",
		22	:	"Color_DarkBlue",
		23	:	"Color_DarkCyan",
	}
}

var colorText map[int]string = colorDict()

	/*Color_DarkGoldenrod GoColor = 0xFFB8860B
	Color_DarkGray GoColor = 0xFFA9A9A9
	Color_DarkGreen GoColor = 0xFF006400
	Color_DarkKhaki GoColor = 0xFFBDB76B
	Color_DarkMagenta GoColor = 0xFF8B008B
	Color_DarkOliveGreen GoColor = 0xFF556B2F
	Color_DarkOrange GoColor = 0xFFFF8C00
	Color_DarkOrchid GoColor = 0xFF9932CC
	Color_DarkRed GoColor = 0xFF8B0000
	Color_DarkSalmon GoColor = 0xFFE9967A
	Color_DarkSeaGreen GoColor = 0xFF8FBC8F
	Color_DarkSlateBlue GoColor = 0xFF483D8B
	Color_DarkSlateGray GoColor = 0xFF2F4F4F
	Color_DarkTurquoise GoColor = 0xFF00CED1
	Color_DarkViolet GoColor = 0xFF9400D3
	Color_DeepPink GoColor = 0xFFFF1493
	Color_DeepSkyBlue GoColor = 0xFF00BFFF
	Color_DimGray GoColor = 0xFF696969
	Color_DodgerBlue GoColor = 0xFF1E90FF
	Color_Firebrick GoColor = 0xFFB22222
	Color_FloralWhite GoColor = 0xFFFFFAF0
	Color_ForestGreen GoColor = 0xFF228B22
	Color_Fuchsia GoColor = 0xFFFF00FF
	Color_Gainsboro GoColor = 0xFFDCDCDC
	Color_GhostWhite GoColor = 0xFFF8F8FF
	Color_Gold GoColor = 0xFFFFD700
	Color_Goldenrod GoColor = 0xFFDAA520
	Color_Gray GoColor = 0xFF808080
	Color_Green GoColor = 0xFF008000
	Color_GreenYellow GoColor = 0xFFADFF2F
	Color_Honeydew GoColor = 0xFFF0FFF0
	Color_HotPink GoColor = 0xFFFF69B4
	Color_IndianRed GoColor = 0xFFCD5C5C
	Color_Indigo GoColor = 0xFF4B0082
	Color_Ivory GoColor = 0xFFFFFFF0
	Color_Khaki GoColor = 0xFFF0E68C
	Color_Lavender GoColor = 0xFFE6E6FA
	Color_LavenderBlush GoColor = 0xFFFFF0F5
	Color_LawnGreen GoColor = 0xFF7CFC00
	Color_LemonChiffon GoColor = 0xFFFFFACD
	Color_LightBlue GoColor = 0xFFADD8E6
	Color_LightCoral GoColor = 0xFFF08080
	Color_LightCyan GoColor = 0xFFE0FFFF
	Color_LightGoldenrodYellow GoColor = 0xFFFAFAD2
	Color_LightGray GoColor = 0xFFD3D3D3
	Color_LightGreen GoColor = 0xFF90EE90
	Color_LightPink GoColor = 0xFFFFB6C1
	Color_LightSalmon GoColor = 0xFFFFA07A
	Color_LightSeaGreen GoColor = 0xFF20B2AA
	Color_LightSkyBlue GoColor = 0xFF87CEFA
	Color_LightSlateGray GoColor = 0xFF778899
	Color_LightSteelBlue GoColor = 0xFFB0C4DE
	Color_Lime GoColor = 0xFF00FF00
	Color_LimeGreen GoColor = 0xFF32CD32
	Color_Linen GoColor = 0xFFFAF0E6
	Color_Magenta GoColor = 0xFFFF00FF
	Color_Maroon GoColor = 0xFF800000
	Color_MediumAquamarine GoColor = 0xFF66CDAA
	Color_MediumBlue GoColor = 0xFF0000CD
	Color_MediumOrchid GoColor = 0xFFBA55D3
	Color_MediumPurple GoColor = 0xFF9370DB
	Color_MediumSeaGreen GoColor = 0xFF3CB371
	Color_MediumSlateBlue GoColor = 0xFF7B68EE
	Color_MediumSpringGreen GoColor = 0xFF00FA9A
	Color_MediumTurquoise GoColor = 0xFF48D1CC
	Color_MediumVioletRed GoColor = 0xFFC71585
	Color_MidnightBlue GoColor = 0xFF191970
	Color_MintCream GoColor = 0xFFF5FFFA
	Color_MistyRose GoColor = 0xFFFFE4E1
	Color_Moccasin GoColor = 0xFFFFE4B5
	Color_NavajoWhite GoColor = 0xFFFFDEAD
	Color_Navy GoColor = 0xFF000080
	Color_OldLace GoColor = 0xFFFDF5E6
	Color_Olive GoColor = 0xFF808000
	Color_OliveDrab GoColor = 0xFF6B8E23
	Color_Orange GoColor = 0xFFFFA500
	Color_OrangeRed GoColor = 0xFFFF4500
	Color_Orchid GoColor = 0xFFDA70D6
	Color_PaleGoldenrod GoColor = 0xFFEEE8AA
	Color_PaleGreen GoColor = 0xFF98FB98
	Color_PaleTurquoise GoColor = 0xFFAFEEEE
	Color_PaleVioletRed GoColor = 0xFFDB7093
	Color_PapayaWhip GoColor = 0xFFFFEFD5
	Color_PeachPuff GoColor = 0xFFFFDAB9
	Color_Peru GoColor = 0xFFCD853F
	Color_Pink GoColor = 0xFFFFC0CB
	Color_Plum GoColor = 0xFFDDA0DD
	Color_PowderBlue GoColor = 0xFFB0E0E6
	Color_Purple GoColor = 0xFF800080
	Color_Red GoColor = 0xFFFF0000
	Color_RosyBrown GoColor = 0xFFBC8F8F
	Color_RoyalBlue GoColor = 0xFF4169E1
	Color_SaddleBrown GoColor = 0xFF8B4513
	Color_Salmon GoColor = 0xFFFA8072
	Color_SandyBrown GoColor = 0xFFF4A460
	Color_SeaGreen GoColor = 0xFF2E8B57
	Color_SeaShell GoColor = 0xFFFFF5EE
	Color_Sienna GoColor = 0xFFA0522D
	Color_Silver GoColor = 0xFFC0C0C0
	Color_SkyBlue GoColor = 0xFF87CEEB
	Color_SlateBlue GoColor = 0xFF6A5ACD
	Color_SlateGray GoColor = 0xFF708090
	Color_Snow GoColor = 0xFFFFFAFA
	Color_SpringGreen GoColor = 0xFF00FF7F
	Color_SteelBlue GoColor = 0xFF4682B4
	Color_Tan GoColor = 0xFFD2B48C
	Color_Teal GoColor = 0xFF008080
	Color_Thistle GoColor = 0xFFD8BFD8
	Color_Tomato GoColor = 0xFFFF6347
	Color_Transparent GoColor = 0xFFFFFFFF
	Color_Turquoise GoColor = 0xFF40E0D0
	Color_Violet GoColor = 0xFFEE82EE
	Color_Wheat GoColor = 0xFFF5DEB3
	Color_White GoColor = 0xFFFFFFFF
	Color_WhiteSmoke GoColor = 0xFFF5F5F5
	Color_Yellow GoColor = 0xFFFFFF00
	Color_YellowGreen GoColor = 0xFF9ACD32 */

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoColor Selector")
	mainwindow.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	mainwindow.SetPadding(40, 40, 40, 40)
	//mainwindow.SetBackgroundColor(ui.Color_Orange)
	mainwindow.Layout().SetBorderStyle(ui.BorderSingleLine)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuHelp := mainmenu.AddMenu("&Help")
	//+++++++++++++
	actionHelp := menuHelp.AddAction("&Help")
	actionHelp.SetOnActivate(ActionHelp_Clicked)
	actionAbout := menuHelp.AddAction("&About")
	actionAbout.SetOnActivate(ActionAbout_Clicked)

	mainlayout := mainwindow.Layout()
	colorboxlayout := mainlayout.AddLayout(ui.VBoxLayout)
	colorboxlayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	colorboxlayout.Show()

	layout1 := colorboxlayout.AddLayout(ui.HBoxLayout)
	layout1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layout1.Show()
	layout2 := colorboxlayout.AddLayout(ui.HBoxLayout)
	layout2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)

	for idx := 1; idx < 4; idx++ {
		color[idx] = ui.GoLabel(layout1, ui.ColorIndex(idx))
		color[idx].SetText(colorText[idx])
		color[idx].SetOnClick(LabelColor_Clicked)
		color[idx].Show()
	}

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}



func ActionAbout_Clicked() {

}

func LabelColor_Clicked(label *ui.GoLabelObj) {
	log.Println("LabelColor_Clicked :", label.Text())
	// copy text to clipboard
	clipboard := app.GoClipBoard()
	err := clipboard.SetText("ui." + label.Text())
	log.Println("ERROR :", err)
}

func ActionExit_Clicked() {
	exitdialog := ui.GoMessageBox(mainwindow, ui.Question, "Closing GoColor Selector.", "Do you really want to exit?", 7, 6)
	exitdialog.SetTitle("GoColor Selector")
	exitdialog.SetBackgroundColor(ui.Color_AliceBlue)
	//exitdialog.CaptionBox().SetBackgroundColor(ui.Color_Gray)
	//exitdialog.CaptionBox().SetPadding(5,5,5,5)
	//exitdialog.CaptionBox().SetBorderStyle(ui.BorderRaised)
	exitdialog.SetCaptionColor(ui.Color_Blue)
	exitdialog.SetMessageColor(ui.Color_Black)
	//exitdialog.MessageBox().SetBorderStyle(ui.BorderSunken)

	button := exitdialog.Exec()
	log.Println("button =", button)
	if button == 6 {
		ui.GoApp().Exit()
	}
	
}

func ActionHelp_Clicked() {

}

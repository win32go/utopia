/* layout.go */

package main

import (
	"log"

	ui "gitlab.com/win32go/win32"
)

type GoLayoutStyle int

const (
	NoLayout 	GoLayoutStyle = iota
	HBoxLayout
	VBoxLayout
	HVBoxLayout
)

func GoLayout(parent GoObject, style GoLayoutStyle) (hLayout *GoLayoutObj) {
	widget := goWidget{
		parentWidget: 	parent,

		sx:       	0,
		sy:       	0,
		swidth:   	300,
		sheight:  	150,

		x:       	0,
		y:       	0,
		width:   	300,
		height:  	150,

		clientWidth: 100,
		clientHeight: 150,

		margin: 		ui.GoMargin(0,0,0,0),
		padding:		ui.GoPadding(0,0,0,0),

		border: 	BorderNone,
	}
	object := goObject{parent, []GoObject{}}
	widget.resize(300, 150)
	widget.setColorPalette()
	l := &GoLayoutObj{widget, object, style, false}
	parent.addControl(l)
	return l
}

type GoLayoutObj struct {
	goWidget
	goObject
	style 		GoLayoutStyle
	topLevel 	bool	
}

/*func (*l layoutObj) controls() (controls Control) {
	return controls
}*/

func (ob *GoLayoutObj) destroy() {
	for _, ctl := range ob.controls {	
		ctl.destroy()				// delete control structure
	}
}

func (ob *GoLayoutObj) draw(pt *ui.GoPainter) {
	log.Println("..........GoLayoutObj.Draw()")
	rcClient := ob.getClientScreenRect()
	rcWind := ob.getWindowScreenRect()
	//log.Println("rcWind : ", rcWind.Left, rcWind.Top, rcWind.Width, rcWind.Height)
	//log.Println("rcClient : ", rcClient.Left, rcClient.Top, rcClient.Width, rcClient.Height)
	//log.Println("rcSize : ", 0, 0, rcClient.Width, rcClient.Height)
	if ob.hasFocus() {
		pt.DrawFocusRectType(rcWind)
	} else {
		pt.SetStockPen(ui.Color_LightGray)
		pt.SetNullBrush()
		pt.DrawRectType(rcClient)
	}
}

func (ob *GoLayoutObj) hasLayout() (ret bool) {
	return true
}

func (ob *GoLayoutObj) objectType() (obtype string) {
	return "GoLayoutObj"
}

func (ob *GoLayoutObj) wid() (widget *goWidget) {
	return &ob.goWidget
}

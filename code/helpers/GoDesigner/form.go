/* form.go */

package main

import (
	"log"
	ui "gitlab.com/win32go/win32"
	//font "gitlab.com/win32go/win32/font"
)

var controlDrag bool = false

func GoForm(preview *ui.GoLayoutObj) (form *GoFormObj) {
	log.Println("..........GoForm() - Entry")

	form = &GoFormObj{
		preview:		preview,
				
		windowRect:		ui.GoRect(),
		clientRect:		ui.GoRect(),
		
		captionHeight:	0,
		iconHeight:		0,
		iconWidth:		0,
		menuHeight:		0,

		windowIcon:		"",
		windowTitle:	"Window - untitled",
		
		hasMenu: 			true,
		hasMinimizeButton:	true,
		hasMaximizeButton:	true,
		hasCloseButton:		true,
		
		layoutStyle:		ui.NoLayout,
		layoutSizePolicy:	&ui.GoSizePolicy{ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false},
		
		
	}
	// Initialise goObject structure
	form.parent = nil
	form.controls = []GoObject{}
	// Initialise goWidget structure
	form.margin = ui.GoMargin(0,0,0,0)
	form.padding = ui.GoPadding(0,0,0,0)
	form.sx = 0
	form.sy = 0		
	form.swidth = 640
	form.sheight = 480
	form.x = 0
	form.y = 0		
	form.width = 640
	form.height = 480
	form.clientWidth = 640
	form.clientHeight = 480

	form.backgroundColor = ui.Color_White



	log.Println("..........GoForm() structure created")

		metrics := app.GoSystemMetrics()

		//desktop := app.GoDeskTop()

		/*log.Println("GoForm() HorizontalSize =", desktop.HorizontalSize())				// 1
		log.Println("GoForm() VerticalSize =", desktop.VerticalSize())					// 1

		log.Println("GoForm() HorizontalRes =", desktop.HorizontalRes())				// 1
		log.Println("GoForm() VerticalRes =", desktop.VerticalRes())					// 1

		log.Println("GoForm() BorderWidth =", metrics.BorderWidth())					// 1
		log.Println("GoForm() BorderHeight =", metrics.BorderHeight())					// 1

		log.Println("GoForm() FixedBorderWidth =", metrics.FixedBorderWidth())			// 3
		log.Println("GoForm() FixedBorderHeight =", metrics.FixedBorderHeight())		// 3

		log.Println("GoForm() ResizeBorderWidth =", metrics.ResizeBorderWidth())		// 4
		log.Println("GoForm() ResizeBorderHeight =", metrics.ResizeBorderHeight())		// 4

		log.Println("GoForm() SmallIconWidth =", metrics.SmallIconWidth())				// 16
		log.Println("GoForm() SmallIconHeight =", metrics.SmallIconHeight())			// 16

		log.Println("GoForm() IconWidth =", metrics.IconWidth())						// 32
		log.Println("GoForm() IconHeight =", metrics.IconHeight())						// 32

		log.Println("GoForm() CaptionHeight =", metrics.CaptionHeight())				// 23

		log.Println("GoForm() CaptionButtonWidth =", metrics.CaptionButtonWidth())		// 36
		log.Println("GoForm() CaptionButtonHeight =", metrics.CaptionButtonHeight())	// 22*/

		log.Println("GoForm() MenuHeight =", metrics.MenuHeight())						// 20

	form.windowRect = ui.GoRect().FromSize(0,0,form.width,form.height)
	
	syscolors := app.GoSystemColors()
	
	form.captionHeight = metrics.CaptionHeight()				// 23
	form.captionHeight = 30

	form.captionButtonWidth = metrics.CaptionButtonWidth()		// 36
	form.captionButtonWidth = 46
	form.captionButtonHeight = metrics.CaptionButtonHeight()	// 22		
	form.captionButtonHeight = 30
	
	form.menuHeight = metrics.MenuHeight()						// 20
	
	form.smallIconHeight = metrics.SmallIconHeight()			// 16
	form.smallIconWidth = metrics.SmallIconWidth()				// 16

	form.iconHeight = metrics.IconHeight()						// 32
	form.iconWidth = metrics.IconWidth()						// 32

	form.titleBarHeight = form.captionHeight

	form.backgroundColor = syscolors.ColorBackground

	return form
}

type GoFormObj struct {
	goObject
	goWidget
	preview 		*ui.GoLayoutObj
	
	windowRect 		*ui.GoRectType
	clientRect		*ui.GoRectType

	captionHeight	int
	captionButtonHeight int
	captionButtonWidth 	int
	smallIconHeight		int
	smallIconWidth		int
	iconHeight		int
	iconWidth		int
	menuHeight		int
	borderHeight 	int
	borderWidth 	int
	fixedBorderHeight 	int
	fixedBorderWidth 	int
	resizeBorderHeight 	int
	resizeBorderWidth 	int
	titleBarHeight		int

	titleBarRect 		*ui.GoRectType
	menuBarRect 		*ui.GoRectType
	iconBoxRect 		*ui.GoRectType
	captionBoxRect 		*ui.GoRectType
	minimizeButtonRect 	*ui.GoRectType
	maximizeButtonRect 	*ui.GoRectType
	closeButtonRect 	*ui.GoRectType
	//minWidth 	int
	//minHeight 	int
	//maxWidth 	int
	//maxHeight 	int
	//irect *goRect
	
	windowIcon 		string
	windowTitle		string

	hasMenu 			bool
	hasMinimizeButton 	bool
	hasMaximizeButton 	bool
	hasCloseButton 		bool


	layoutStyle			ui.GoLayoutStyle
	layoutSizePolicy 	*ui.GoSizePolicy

	
}

func(f *GoFormObj) Paint(painter *ui.GoPainter) {
	f.draw(painter)
}

func (f *GoFormObj)BlitTemplate(template *GoTemplateObj) {

}

func(f *GoFormObj) draw(painter *ui.GoPainter) {
	log.Println("..........GoForm.Draw()")
	// load system vatiable font ( "Segoe UI", 16pt, normal weight )
	systemFont, err := ui.CreateFont("Segoe UI", 16, ui.FW_Normal, false)
	if err != nil {
		log.Println("..........FAILED to CreateFont Segoe_Ui", err)
	}

	// Draw TitleBar

	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.titleBarRect)

	// Draw CaptionText

	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.captionBoxRect)
	//rc := f.captionBoxRect
	//log.Println("CaptionBoxRect(", rc.Left(), rc.Top(), rc.Width(), rc.Height(), " )")
	//textRect := f.captionBoxRect
	//textRect.SetWidth(textRect.Width())
	//textRect.SetX(textRect.X() + f.iconBoxRect.Width())

	painter.SetFont(systemFont)
	painter.DrawTextLine(f.windowTitle, f.captionBoxRect, ui.AlignLeft, ui.AlignVCenter)
	painter.RestoreFont()

	// Draw Icon

	sx := f.iconBoxRect.Left() + ((f.captionBoxRect.Height()) / 4) + 1
	sy := f.iconBoxRect.Top() + ((f.captionBoxRect.Height()) / 4) - 1
	painter.SetStockPen(ui.Color_Orange)
	painter.SetStockBrush(ui.Color_Orange)
	painter.DrawRect(sx, sy, f.smallIconWidth, f.smallIconHeight)

	// Draw Minimize Button

	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.minimizeButtonRect)

	sx = ((f.minimizeButtonRect.Left() + f.minimizeButtonRect.Right()) / 2) - ((f.captionButtonHeight - 8) / 4)
	sy = (f.minimizeButtonRect.Top() + f.minimizeButtonRect.Bottom()) / 2 	//((f.captionButtonHeight - 2) / 4)
	ex := sx + ((f.captionButtonHeight - 8) / 2)
	ey := sy
	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_Black)
	painter.DrawLine(sx, sy, ex, ey)

	// Draw Maximize Button
	
	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.maximizeButtonRect)

	sx += f.captionButtonWidth
	ex += f.captionButtonWidth
	sy -= (f.captionButtonHeight - 8) / 4
	ey += (f.captionButtonHeight - 8) / 4
	painter.SetNullBrush()
	painter.SetStockPen(ui.Color_Black)
	painter.DrawRect(sx, sy, ex - sx - 1, ey - sy)
	
	// Draw Close Button

	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.closeButtonRect)

	painter.SetStockBrush(ui.Color_Black)
	painter.SetStockPen(ui.Color_Black)
	sx += f.captionButtonWidth
	ex += f.captionButtonWidth
	painter.DrawLine(sx + 1, sy + 1, ex, ey + 1)
	painter.DrawLine(sx + 1, ey, ex, sy)

	// Draw MenuBar

	if f.hasMenu {
		painter.SetStockBrush(ui.Color_AliceBlue)
		painter.SetStockPen(ui.Color_White)
		painter.DrawRectType(f.menuBarRect)
	}

	// Draw Client Area backgound

	painter.SetStockBrush(ui.Color_White)
	painter.SetStockPen(ui.Color_White)
	painter.DrawRectType(f.clientRect)

	// Draw all layouts and controls
	//log.Println("..........GoForm.Draw()")
	painter.ClipRect(f.clientRect)
	for _, ctrl := range f.getControls() {
		if ctrl.hasLayout() {
			log.Println("..........Control is layout")
			f.drawLayout(painter, ctrl)
			painter.ClipRect(f.clientRect)
		} else {
			ctrl.draw(painter)
		}
	}
}

func (f *GoFormObj) drawLayout(painter *ui.GoPainter, layout GoObject) {
	layout.draw(painter)
	painter.ClipRect(layout.getClientScreenRect())
	for _, ctrl := range layout.getControls() {
		if ctrl.hasLayout() {
			f.drawLayout(painter, ctrl)
			painter.ClipRect(ctrl.getClientScreenRect())
		} else {
			ctrl.draw(painter)
		}  
	}
	
}

func(f *GoFormObj) MouseDown(button ui.GoButtonState, x int, y int) {
	if mouseAction == addingControl {
		parent := f.GetControlAt(x, y)
		log.Println("..........GoForm.MouseDown() - Parent =", parent)

		if parent != nil {
			switch template.templateType {
				case LayoutTemplate:
					layout := GoLayout(parent, HBoxLayout)
					layout.moveTo(x, y)

					//parent.repack()

				case ButtonTemplate:
					button := GoButton(parent, "Button")
					button.moveTo(x, y)
				case RadioButtonTemplate:
					radiobutton := GoRadioButton(parent, "Radio Button")
					radiobutton.moveTo(x, y)
				case CheckBoxTemplate:
					checkbox := GoCheckBox(parent, "Checkbox")
					checkbox.moveTo(x, y)
				default:
			}

		}
		mouseAction = idleControl
		f.Refresh()
	} else {
		control := f.GetControlAt(x, y)
		if control != nil {
			if control.hasFocus() {
				mouseAction = movingControl
				dragControl = control
				dragStartX = x
				dragStartY = y
			}
		}
	}
}

func(f *GoFormObj) MouseMove(x int, y int) {
	//log.Println("..........Form MouseMove mouseAction =", mouseAction)
	if mouseAction == movingControl {
		//log.Println("..........Form MouseMove")
			moveX := x - dragStartX
			moveY := y - dragStartY
			if dragControl.objectType() == "GoLayoutObj" {
				f.moveContainer(dragControl, moveX, moveY)
			} else {
				dragControl.move(moveX, moveY)
			}
			dragStartX = x
			dragStartY = y
			f.Refresh()
		}
}

func(f *GoFormObj) MouseUp(button ui.GoButtonState, x int, y int) {
	control := f.GetControlAt(x, y)
	if control != nil {
		//if control.objectType() == "GoLayoutObj" {
			if mouseAction == movingControl {
				//moveX := x - dragStartX
				//moveY := y - dragStartY
				//dragControl.move(moveX, moveY)
				//dragControl.setParent(control)
				mouseAction = idleControl
				controlWindow.Refresh()
			} else if mouseAction == addingControl {
				//moveX := x - dragStartX
				//moveY := y - dragStartY
				//dragControl.move(moveX, moveY)
				//dragControl.setParent(control)
				mouseAction = idleControl
				controlWindow.Refresh()

			} else if mouseAction == sizingControl {
				// get sizing node new position recalculate control position and / or size
			}
		//}
		f.repack(nil)
		f.ChangeControlFocus(control)
		f.UpdateProperties(control)
		f.Refresh()
	}
}

func(f *GoFormObj) Refresh() {
	preview.Refresh()
	//previewWindow.Refresh()
}

func(f *GoFormObj) Resize(rect *ui.GoRectType) {
	log.Println("..........GoForm.Resize()")
	// calculate new form size
	
	f.width = rect.Width()
	f.height = rect.Height()
	
	width := f.width
	log.Println("width =", width)
	height := f.height - f.captionHeight
	log.Println("height =", height)

	f.windowRect = ui.GoRect().FromSize(0, 0, width, height)

	f.titleBarRect = ui.GoRect().FromSize(0, 0, width, f.captionHeight)
	
	f.iconBoxRect = ui.GoRect().FromSize(0, 0, (f.smallIconWidth * 2) - 2, f.captionHeight)
	
	f.captionBoxRect = ui.GoRect().FromSize(f.iconBoxRect.Width(), 0, width - f.iconBoxRect.Width(), f.captionHeight)
	
	f.minimizeButtonRect = ui.GoRect().FromSize(f.width - f.captionButtonWidth * 3, 0, f.captionButtonWidth, f.captionButtonHeight)
	f.maximizeButtonRect = ui.GoRect().FromSize(f.width - f.captionButtonWidth * 2, 0, f.captionButtonWidth, f.captionButtonHeight)
	f.closeButtonRect = ui.GoRect().FromSize(f.width - f.captionButtonWidth, 0, f.captionButtonWidth, f.captionButtonHeight)
	if f.hasMenu == true {
		f.menuBarRect = ui.GoRect().FromSize(0, f.titleBarRect.Bottom(), f.width, f.menuHeight)
		f.clientRect = ui.GoRect().FromSize(0, f.menuBarRect.Bottom(), f.width, f.height - f.menuBarRect.Bottom())
	} else {
		f.clientRect = ui.GoRect().FromSize(0, f.titleBarRect.Bottom(), f.width, f.height - f.titleBarRect.Bottom())
	}
}

// Get Window DPI
//func getWindowDPI()
func scaletoDPI(value int) (scaledValue int){
	dpi := app.GoDeskTop().HorizontalRes()
	return int(value * dpi / 96)
}

//func titleBarRect(hWnd w32.HWND, object string) (rect *GoRectTyp) {
	/*w32.HTHEME*/
	/*top_and_bottom_borders := 2
	 theme := w32.OpenThemeData(hWnd, object)
	cx, cy := w32.GetThemePartSize(theme, 0, w32.WP_CAPTION, w32.CS_ACTIVE, 0, w32.TS_TRUE)
	w32.CloseThemeData(theme)

	height := scaleToDPI(cy) + top_and_bottom_borders

	rect := w32.GetClientRect(hWnd)
	rect.bottom = rect.top + iconHeight
	return rect*/
//}

func (f *GoFormObj) ChangeControlFocus(control GoObject) {
	for _, ctrl := range f.getControls() {
		if ctrl.hasLayout() {
			log.Println("ChangeControlFocus::Control hasLayout(TRUE)")
			ret := f.changeFocus(ctrl, control)
			if ret == false {
				if ctrl == control {
					log.Println("ChangeControlFocus::Layout.setFocus(TRUE)")
					ctrl.setFocus(true)
				} else {
					log.Println("Control setFocus(FALSE)")
					ctrl.setFocus(false)
				}
			}
		} else {
			if ctrl == control {
				log.Println("Control setFocus(TRUE)")
				ctrl.setFocus(true)
			} else {
				log.Println("Control setFocus(FALSE)")
				ctrl.setFocus(false)
			}
		}
	}
	f.Refresh()
}

func (f *GoFormObj) changeFocus(layout GoObject, control GoObject) (selected bool) {
	selected = false
	for _, ctrl := range layout.getControls() {
		if ctrl.hasLayout() {
			log.Println("changeFocus::Control hasLayout(TRUE)")
			ret := f.changeFocus(ctrl, control)
			if ret == false {
				if ctrl == control {
					log.Println("changecFocus::Layout.setFocus(TRUE)")
					ctrl.setFocus(true)
					selected = true
				} else {
					log.Println("changeFocus::Control setFocus(FALSE)")
					ctrl.setFocus(false)
				}
			}
		} else {
			if ctrl == control {
				log.Println("changeFocus::Control setFocus(TRUE)")
				ctrl.setFocus(true)
				selected = true
			} else {
				log.Println("changeFocus::Control setFocus(FALSE)")
				ctrl.setFocus(false)
			}
		}
		
	}
	log.Println("changeFocus::return:", selected)
	return selected
}

func (f *GoFormObj) repack(layout *GoLayoutObj) {
	if layout == nil {
		// repack main forme layout

	}
}


func (f *GoFormObj) GetControlAt(x int, y int) (control GoObject) {
	for _, ctrl := range f.controls {
		if ctrl.containsPoint(x, y) == true {
			if ctrl.hasLayout() == true {
				return f.getObjectFrom(ctrl, x, y)
			}
			return ctrl
		}  
	}
	return f
}

func (f *GoFormObj) SetControlSelected(control GoObject, selected bool) {

}

func (f *GoFormObj) UpdateProperties(obj GoObject) {
	propertiesWindow.SetObjectProperties(obj)
}

func (f *GoFormObj) containsPoint(x int, y int) bool {
	return x >= f.x && y >= f.y && x < f.x + f.width && y < f.y + f.height
}

func (f *GoFormObj) getObjectFrom(parent GoObject, x int, y int) (obj GoObject) {
	for _, obj := range parent.getControls() {
		log.Println("obj.containsPoint(", x, y, ")")
		if obj.containsPoint(x, y) == true {
			log.Println("obj.containsPoint = true")

			if obj.hasLayout() == true{
				return f.getObjectFrom(obj, x, y)
			}
			return obj
		}
		log.Println("obj.containsPoint = false")
	}
	return parent
}

func (f *GoFormObj) moveContainer(container GoObject, moveX int, moveY int) {
	container.move(moveX, moveY)
	for _, obj := range container.getControls() {
		if obj.objectType() == "GoLayoutObj" {
			f.moveContainer(obj, moveX, moveY)
		} else {
			obj.move(moveX, moveY)
		}
	}
}
func (f *GoFormObj) hasLayout() (ret bool) {
	return true
}

func (f *GoFormObj) objectType() (objtype string) {
	return "GoFormObj"
}

func (f *GoFormObj) wid() (widget *goWidget) {
	return &f.goWidget
}
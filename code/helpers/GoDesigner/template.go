/* template.go */

package main

import (
	_ "log"
	ui "gitlab.com/win32go/win32"
)

type GoTemplateType int

const (
	LayoutTemplate 	GoTemplateType = iota
	ButtonTemplate
	CheckBoxTemplate
	RadioButtonTemplate
)

func GoTemplate(templateType GoTemplateType) (template *GoTemplateObj) {
	switch templateType {
		case LayoutTemplate:
			return &GoTemplateObj{templateType, 0, 0, 200, 50, "", app.ColorWindowText, app.ColorHighlightText}
		case ButtonTemplate:
			return &GoTemplateObj{templateType, 0, 0, 80, 24, "", app.ColorWindowText, app.ColorHighlightText}
		case RadioButtonTemplate:
			return &GoTemplateObj{templateType, 0, 0, 140, 20, "", app.ColorWindowText, app.ColorHighlightText}
		case CheckBoxTemplate:
			return &GoTemplateObj{templateType, 0, 0, 140, 20, "", app.ColorWindowText, app.ColorHighlightText}
		default:
	}
	return nil
}

type GoTemplateObj struct {
	templateType  GoTemplateType
	x       	int
	y       	int
	width   	int
	height  	int
	focus 		string
	forecolor 		ui.GoColor
	highlightText 	ui.GoColor
}

func (t *GoTemplateObj) blit(pt *ui.GoPainter) {
	//savedRop2 := pt.SetRop2(ui.NotXorRop)
	/*if savedRop2 == 0 {
		log.Println("GoPainter::SetROP2 FAILED...............")
		return
	}*/
	pt.DrawFocusRect(t.x, t.y, t.width, t.height)
	/*rect := ui.GoRect().FromSize(t.x, t.y, t.width, t.height)
	//rc := rect.DeductPadding(ui.GoPadding(5, 5, 5, 5))
	pt.SetNullBrush()
	pt.SetStockPen(t.forecolor)
	switch t.templateType {
		case LayoutTemplate:
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		case ButtonTemplate:
			//pt.SetBackMode(ui.TransparentMode)
			//pt.SetTextColor(t.forecolor)
			//pt.SetStockPen(t.forecolor)
			//pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
			//pt.DrawTextLine("Push Button", rc, ui.AlignCenter, ui.AlignVCenter)  
		case RadioButtonTemplate:
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		case CheckBoxTemplate:
			pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
		default:
	}*/
	//pt.SetRop2(ui.GoRasterOp(savedRop2))
}

func (t *GoTemplateObj) move(x int, y int) {
	t.x += x
	t.y += y
}

func (t *GoTemplateObj) moveTo(x int, y int) {
	t.x = x
	t.y = y
}
/* button.go */

package main

import (
	"log"

	ui "gitlab.com/win32go/win32"
)


func GoButton(parent GoObject, text string) (hButton *GoButtonObj) {
	widget := goWidget{
		parentWidget: 	parent,

		sx:       	0,
		sy:       	0,
		swidth:   	100,
		sheight:  	30,

		x:       	0,
		y:       	0,
		width:   	100,
		height:  	30,

		clientWidth: 100,
		clientHeight: 30,

		minWidth: 	50,
		minHeight: 	15,
		maxWidth: 	100,
		maxHeight: 	30,

		disabled: 	false,
		visible:   	false,
		focus: 		false,
		autoSize:	false,
		border: 	BorderNone,

		font:       nil,

		margin: 	ui.GoMargin(0, 0, 0, 0),			// clear margin surrounding control
		padding:  	ui.GoPadding(5, 5, 5, 5),			// padding surrounding control content
		sizePolicy:	GoSizePolicy(FixedWidth, FixedHeight, true),			// control sizing policy - GoSizePolicy{horiz, vert, fixed}
		spacing: 	0,
		text: 	 	"Button",
	}
	widget.setColorPalette()
	object := goObject{parent, []GoObject{}}
	ob := &GoButtonObj{widget, object, 0}
	parent.addControl(ob)
	return ob
}

type GoButtonObj struct {
	goWidget
	goObject
	groupIdx int
	//autoSize bool
	//onClick func()
	//onDisable func()
	//onDoubleClick func()
	//onHiLite func()
	//onPaint func()
	//onUnHiLite func()
}

func (ob *GoButtonObj) draw(pt *ui.GoPainter) {
	log.Println("..........GoButtonObj.Draw()")
	
	rect := ob.getClientScreenRect()
	rc := rect.DeductPadding(ob.padding)
	pt.SetBackMode(ui.TransparentMode)
	pt.SetTextColor(ob.buttonText)
	pt.SetStockBrush(ob.faceColor)
	pt.SetStockPen(ob.faceColor)
	pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
	pt.DrawTextLine(ob.text, rc, ui.AlignHCenter, ui.AlignVCenter)
	if ob.hasFocus() {
		pt.DrawFocusRect(ob.sx, ob.sy, ob.swidth, ob.sheight)
	}  

}

func (ob *GoButtonObj) groupId() (id int) {
	return ob.groupIdx
}

func (ob *GoButtonObj) isLayout() (ret bool) {
	return false
}

func (ob *GoButtonObj) objectType() (obtype string) {
	return "GoButtonObj"
}

func (ob *GoButtonObj) wid() (widget *goWidget) {
	return &ob.goWidget
}
/* object.go */

package main

import (
	_ "log"

	ui "gitlab.com/win32go/win32"
)

type GoObject interface {
	addControl(control GoObject)			// implemented in goObject code
	//control(id int) (GoObject)
	getControls() ([]GoObject)				// implemented in goObject code
	getClientRect() *ui.GoRectType			// implemented in goWidget code
	getClientScreenRect() *ui.GoRectType	// implemented in goWidget code
	getWindowRect() *ui.GoRectType			// implemented in goWidget code
	getWindowScreenRect() *ui.GoRectType	// implemented in goWidget code
	containsPoint(x int, y int) (bool)		// implemented in goWidget code
	draw(*ui.GoPainter)						// implemented in control code / no default
	
	groupId() (int)							// implemented in control code / default 0 goObject code
	hasFocus() (bool)						// implemented in goWidget code
	hasLayout() (bool)						// implemented in control code / defaults goObject code
	objectType() (string)					// implemented in control code / no default
	move(x int, y int)						// implemented in goWidget code
	parentControl() (control GoObject)		// implemented in goObject code
	removeControl(control GoObject)			// implemented in goObject code
	setFocus(focus bool)					// implemented in goWidget code
	destroy()								// implemented in control code / defaults goObject code
	wid() (widget *goWidget)				// implemented in control code / no default
}

type goObject struct {
	parent GoObject
	controls []GoObject
}

func (obj *goObject) addControl(control GoObject) {
	obj.controls = append(obj.controls, control)
}

func (obj *goObject) getControls() (objects []GoObject) {
	return obj.controls
}

func (obj *goObject) parentControl() (control GoObject) {
	return obj.parent
}

func (obj *goObject) removeControl(control GoObject) {
	k := 0
	for _, v := range obj.controls {
	    if v != control {
	        obj.controls[k] = v
	        k++
	    }
	}
	obj.controls = obj.controls[:k] // set slice len to remaining elements
}

// default implementation
func (obj *goObject) destroy() {
	
}

// default implementation
func (obj *goObject) groupId() (id int) {
	return 0
}

// default implementation
func (obj *goObject) hasLayout() (ret bool) {
	return false
}
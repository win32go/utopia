/* checkbox.go */

package main

import (
	"log"

	ui "gitlab.com/win32go/win32"
)

func GoCheckBox(parent GoObject, text string) (hCheckBox *GoCheckBoxObj) {
	widget := goWidget{
		parentWidget: 	parent,
		
		sx:       	0,
		sy:       	0,
		swidth:   	200,
		sheight:  	20,

		x:      	0,
		y:      	0,
		width:  	200,
		height: 	20,

		clientWidth: 200,
		clientHeight: 20,

		minWidth: 	50,
		minHeight: 	15,
		maxWidth: 	100,
		maxHeight: 	30,

		disabled: 	false,
		visible:   	false,
		focus: 		false,
		autoSize:	false,
		border: 	BorderNone,

		font:       nil,

		margin: 	ui.GoMargin(0, 0, 0, 0),			// clear margin surrounding control
		padding:  	ui.GoPadding(5, 5, 5, 5),			// padding surrounding control content
		sizePolicy:	GoSizePolicy(FixedWidth, FixedHeight, true),			// control sizing policy - GoSizePolicy{horiz, vert, fixed}
		spacing: 	0,
		text: 	 	"CheckBox",
		
	}
	widget.setColorPalette()
	object := goObject{parent, []GoObject{}}
	ob := &GoCheckBoxObj{widget, object, 0}
	parent.addControl(ob)
	return ob
}

type GoCheckBoxObj struct {
	goWidget
	goObject
	groupIdx int
	//autoSize bool
	//onClick func()
	//onDisable func()
	//onDoubleClick func()
	//onHiLite func()
	//onPaint func()
	//onUnHiLite func()
}

func (ob *GoCheckBoxObj) draw(pt *ui.GoPainter) {
	log.Println("..........GoCheckBoxObj.Draw()")
	rect := ob.getClientScreenRect()
	rc := rect.DeductPadding(ob.padding)
	pt.SetTextColor(ob.buttonText)
	pt.SetStockBrush(ob.faceColor)
	pt.SetStockPen(ob.faceColor)
	pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
	pt.DrawTextLine(ob.text, rc, ui.AlignHCenter, ui.AlignVCenter)   
	if ob.hasFocus() {
		pt.DrawFocusRect(ob.sx, ob.sy, ob.swidth, ob.sheight)
	}
}

func (ob *GoCheckBoxObj) groupId() (id int) {
	return ob.groupIdx
}

func (ob *GoCheckBoxObj) isLayout() (ret bool) {
	return false
}

func (ob *GoCheckBoxObj) objectType() (obtype string) {
	return "GoCheckBoxObj"
}

func (ob *GoCheckBoxObj) wid() (widget *goWidget) {
	return &ob.goWidget
}
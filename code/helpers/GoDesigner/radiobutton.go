/* radiobutton.go */

package main

import (
	"log"
	ui "gitlab.com/win32go/win32"
)

func GoRadioButton(parent GoObject, text string) (hRadioButton *GoRadioButtonObj) {
	widget := goWidget{
		parentWidget: 	parent,
		
		sx:       	0,
		sy:       	0,
		swidth:   	200,
		sheight:  	20,

		x:      	0,
		y:      	0,
		width:  	200,
		height: 	20,

		clientWidth: 200,
		clientHeight: 20,

		minWidth: 	50,
		minHeight: 	15,
		maxWidth: 	100,
		maxHeight: 	30,

		disabled: 	false,
		visible:   	false,
		focus: 		false,
		autoSize:	false,
		border: 	BorderNone,

		font:       nil,

		margin: 	ui.GoMargin(0, 0, 0, 0),			// clear margin surrounding control
		padding:  	ui.GoPadding(5, 5, 5, 5),			// padding surrounding control content
		sizePolicy:	GoSizePolicy(FixedWidth, FixedHeight, true),			// control sizing policy - GoSizePolicy{horiz, vert, fixed}
		spacing: 	0,
		text: 	 	"RadioButton",
		
	}
	widget.setColorPalette()
	object := goObject{parent, []GoObject{}}
	ob := &GoRadioButtonObj{widget, object, 0}
	parent.addControl(ob)
	return ob
}

type GoRadioButtonObj struct {
	goWidget
	goObject
	groupIdx int
	
}

func (ob *GoRadioButtonObj) draw(pt *ui.GoPainter) {
	log.Println("..........GoRadioButtonObj.Draw()")
	rect := ob.getClientScreenRect()
	rc := rect.DeductPadding(ob.padding)
	pt.SetTextColor(ob.buttonText)
	pt.SetStockBrush(ob.faceColor)
	pt.SetStockPen(ob.faceColor)
	pt.DrawRect(rect.X(), rect.Y(), rect.Width(), rect.Height())
	pt.DrawTextLine(ob.text, rc, ui.AlignHCenter, ui.AlignVCenter) 
	if ob.hasFocus() {
		pt.DrawFocusRect(ob.sx, ob.sy, ob.swidth, ob.sheight)
	} 

}

func (ob *GoRadioButtonObj) groupId() (id int) {
	return ob.groupIdx
}

func (ob *GoRadioButtonObj) isLayout() (ret bool) {
	return false
}

func (ob *GoRadioButtonObj) objectType() (obtype string) {
	return "GoRadioButtonObj"
}

func (ob *GoRadioButtonObj) wid() (widget *goWidget) {
	return &ob.goWidget
}
/* widget.go */

package main

import (
	_ "log"

	ui "gitlab.com/win32go/win32"
)

type GoSizeType int

const(
	FixedWidth GoSizeType		= 0x0000
	FixedHeight GoSizeType		= 0x0001
	MinimumWidth GoSizeType		= 0x0002
	MinimuHeight GoSizeType		= 0x0004
	MaximumWidth GoSizeType		= 0x0008
	MaximumHeight GoSizeType	= 0x0010
	PreferredWidth GoSizeType	= 0x0020
	PreferredHeight GoSizeType	= 0x0040
	ExpandingWidth GoSizeType	= 0x0080
	ExpandingHeight GoSizeType 	= 0x0100
)

type GoSizePolicyType struct {
	Horiz 	GoSizeType
	Vert 	GoSizeType
	Fixed 	bool
}

func GoSizePolicy(horiz GoSizeType, vert GoSizeType, fixed bool) (*GoSizePolicyType) {
	return &GoSizePolicyType{horiz, vert, fixed}
}

type GoBorderType int

const (
	BorderNone GoBorderType = iota
	BorderSingleLine
	BorderSunken
	BorderSunkenThick
	BorderRaised
)

/*func GoMargin(l, t, r, b int) *GoMarginType {
	return &GoMarginType{l, t, r, b}
}

type GoMarginType struct {
	Left 	int
	Top 	int
	Right 	int
	Bottom 	int
}

func (m *GoMarginType) Height() int{
	return m.Top + m.Bottom
}

func (m *GoMarginType) Width() int{
	return m.Left + m.Right
}

func GoPadding(l, t, r, b int) *GoPaddingType {
	return &GoPaddingType{l, t, r, b}
}

type GoPaddingType struct {
	Left 	int
	Top 	int
	Right 	int
	Bottom 	int
}

func (m *GoPaddingType) Height() int{
	return m.Top + m.Bottom
}

func (m *GoPaddingType) Width() int{
	return m.Left + m.Right
}*/

type goWidget struct {
	parentWidget		GoObject
	
	// position of window in screen coords
	sx 					int 						// screen coordinates
	sy 					int 						// screen coordinates
	swidth 				int 						// screen coordinates
	sheight 			int 						// screen coordinates

	// position of client rect in screen coords
	clientOffsetX				int					// screen coordinates
	clientOffsetY				int					// screen coordinates

	// position and size of window in client coords
	x       			int							// client coordinates
	y       			int							// client coordinates
	width   			int							// client coordinates
	height  			int							// client coordinates

	// position and size of client rect in parent client coords
	clientX 			int							// client coordinates Always 0
	clientY 			int							// client coordinates Always 0
	clientWidth 		int							// client coordinates
	clientHeight 		int							// client coordinates

	minWidth 			int
	minHeight 			int
	maxWidth 			int
	maxHeight 			int

	margin 				*ui.GoMarginType 			// clear margin surrounding control
	padding  			*ui.GoPaddingType			// padding surrounding control content
	sizePolicy			*GoSizePolicyType			// control sizing policy - GoSizePolicy{horiz, vert, fixed}
	spacing 			int

	autoSize			bool
	disabled 			bool
	focus 				bool
	visible   			bool

	backgroundImage 	*ui.GoImageObj
	border 				GoBorderType
	font        		*ui.GoFont
	text 	 			string

	alpha 				uint8
	backColor 			ui.GoColor 	// COLOR_WINDOW
	backgroundColor 	ui.GoColor 	// COLOR_WINDOW
	buttonText 			ui.GoColor 	// COLOR_BTNTEXT Foreground color for button text.
	faceColor			ui.GoColor 	// COLOR_BTNFACE Background color for button.
	foreColor 			ui.GoColor 	// COLOR_WINDOWTEXT
	grayText 			ui.GoColor 	// COLOR_GRAYTEXT Foreground color for disabled button text.
	highlight 			ui.GoColor 	// COLOR_HIGHLIGHT Background color for slected button.
	highlightText 		ui.GoColor 	// COLOR_HIGHLIGHTTEXT Foreground color for slected button text.
	hotlight 			ui.GoColor 	// COLOR_HOTLIGHT Hyperlink color.
}

func (w *goWidget) borderSize() int {
	switch w.border {
		case BorderNone:
			return 0
		case BorderSingleLine:
			return 1
		case BorderSunken:
			return 2
		case BorderSunkenThick:
			return 4
		case BorderRaised:
			return 4
	}
	return 0
}

func (w *goWidget) containsPoint(x int, y int) bool {
	return x >= w.sx && y >= w.sy && x < w.sx + w.swidth && y < w.sy + w.sheight
}

/*func (w *goWidget) getInnerRect() *ui.GoRectType {
	return ui.GoRect().FromSize(w.clientX, w.clientY, w.clientWidth, w.clientHeight)
}*/

func (w *goWidget) getClientRect() *ui.GoRectType {
	return ui.GoRect().FromSize(w.clientX, w.clientY, w.clientWidth, w.clientHeight)
}

func (w *goWidget) getClientScreenRect() *ui.GoRectType {
	return ui.GoRect().FromSize(w.clientOffsetX, w.clientOffsetY, w.clientWidth, w.clientHeight)
}

func (w *goWidget) getWindowRect() *ui.GoRectType {
	return ui.GoRect().FromSize(w.x, w.y, w.width, w.height)
}

func (w *goWidget) getWindowScreenRect() *ui.GoRectType {
	return ui.GoRect().FromSize(w.sx, w.sy, w.swidth, w.sheight)
}

func (w *goWidget) hasFocus() (focus bool) {
	return w.focus
}

func (w *goWidget) move(x int, y int)  {
	w.sx += x
	w.sy += y
	w.clientOffsetX += x
	w.clientOffsetY += y
	w.x = w.sx - w.parentWidget.wid().clientOffsetX
	w.y = w.sy - w.parentWidget.wid().clientOffsetY
}

func (w *goWidget) moveTo(x int, y int)  {
	w.sx = x
	w.sy = y
	w.clientOffsetX = x + w.borderSize()// + w.margin.Left
	w.clientOffsetY = y + w.borderSize()// + w.margin.Top
	w.x = x - w.parentWidget.wid().x
	w.y = y - w.parentWidget.wid().y
}

func (w *goWidget) resize(width int, height int) {
	w.swidth = width
	w.sheight = height
	w.clientWidth = w.swidth - w.borderSize() * 2 //- w.margin.Width()
	w.clientHeight = w.sheight - w.borderSize() * 2 //- w.margin.Height()
}

func (w *goWidget) setColorPalette()  {
// Create Widget Palette
	w.backColor = app.ColorWindow				// Background used for painting text, patterns, etc.
	w.backgroundColor = app.ColorWindow 		// Background of pages, panes, popups, and windows.
	w.buttonText = app.ColorButtonText 		// Foreground color for button text and ui.
	w.faceColor = app.Color3DFace  			// Background color for button and ui.
	w.foreColor = app.ColorWindowText 		// Headings, body copy, lists, placeholder text,
												// app and window borders, any UI that can't be interacted with.
	w.grayText = app.ColorGrayText 			// Foreground color for disabled button text.
	w.highlight = app.ColorHighlight 			// Background color for slected button or active ui.
	w.highlightText = app.ColorHighlightText 	// Foreground color for slected button text or active ui.
	w.hotlight = app.ColorHotlight 			// Hyperlink color.
}

func (w *goWidget) setFocus(focus bool) {
	w.focus = focus
}

/* goproperties.go */

package main

import (
	"log"
	_ "os"
	_ "path/filepath"
	str "strconv"

	ui "gitlab.com/win32go/win32"

)


func GoPropertiesWindow(parent ui.GoObject) (pWindow *GoPropertiesObj) {
	window := ui.GoWindow(parent)
	pWin := &GoPropertiesObj{*window, nil, nil, []*ui.GoLabelObj{}, []*ui.GoLineEditObj{}, make(map[string] int), []string{}}
	pWin.SetTitle("GoDesigner")
	pWin.SetPosition(1200, 30)
	pWin.SetSize(250, 600)
	pWin.SetLayoutStyle(ui.VBoxLayout)
	pWin.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	pWin.SetBackgroundColor(ui.Color_AliceBlue)
	pWin.CreateLayout()
	pWin.CreatePropertyGroups()
	
	/*for i := 0; i < len(pWin.properties); i++ {

		pWin.value[pWin.properties[i]] = 0
	}*/

	//propertiesLWindow.SetObjectProperties(form)
	return pWin
}

type GoPropertiesObj struct {
	ui.GoWindowObj	// embedded widget
	propertyNameLayout *ui.GoLayoutObj
	propertyValueLayout *ui.GoLayoutObj

	propertyLabel 		[]*ui.GoLabelObj
	propertyValue		[]*ui.GoLineEditObj
	 				
	property 			map[string] int

	widgetProperties 	[]string

}


func (pw *GoPropertiesObj) CreateLayout() {

	// Selected controls properties

	propertiesLayout := pw.Layout()
	propertiesLayout.SetSpacing(0)

	propertiesCaption := ui.GoLabel(propertiesLayout, " Property Editor")
	propertiesCaption.SetSize(200, 24)
	propertiesCaption.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//propertiesCaption.SetBorderStyle(ui.BorderSingleLine)
	propertiesCaption.SetTextColor(ui.Color_White)
	propertiesCaption.SetBackgroundColor(ui.Color_LightGray)
	propertiesCaption.Show()

	propertyTableLayout := ui.GoLayout(propertiesLayout, ui.HBoxLayout)
	propertyTableLayout.SetSpacing(0)
	propertyTableLayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	propertyTableLayout.Show()

	pw.propertyNameLayout = ui.GoLayout(propertyTableLayout, ui.VBoxLayout)
	pw.propertyNameLayout.SetPadding(0,2,0,2)
	pw.propertyNameLayout.SetSpacing(0)
	pw.propertyNameLayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//propertyNameLayout.SetBorderStyle(ui.BorderSingleLine)
	pw.propertyNameLayout.Show()

	pw.propertyValueLayout = ui.GoLayout(propertyTableLayout, ui.VBoxLayout)
	pw.propertyValueLayout.SetPadding(0,2,0,2)
	pw.propertyValueLayout.SetSpacing(0)
	pw.propertyValueLayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//propertyValueLayout.SetBorderStyle(ui.BorderSingleLine)
	pw.propertyValueLayout.Show()
}

func (pw *GoPropertiesObj) CreatePropertyGroups() {
	pw.widgetProperties = []string{"X", "Y", "Width", "Height", "OffsetX", "OffsetY", "ClientX", "ClientY", "ClientWidth", "ClientHeight", "MarginLeft", "MarginTop", "MarginRight", "MarginBottom", "PaddingLeft", "PaddingTop", "PaddingRight", "PaddingBottom"}
	pw.propertyLabel = make([]*ui.GoLabelObj, len(pw.widgetProperties)) 
	pw.propertyValue = make([]*ui.GoLineEditObj, len(pw.widgetProperties))
	for i := 0; i < len(pw.widgetProperties); i++ {
		pw.propertyLabel[i] = ui.GoLabel(pw.propertyNameLayout, pw.widgetProperties[i])
		pw.propertyLabel[i].SetSize(200, 20)
		pw.propertyLabel[i].SetMargin(0,0,0,0)
		pw.propertyLabel[i].SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
		pw.propertyLabel[i].SetBorderStyle(ui.BorderSingleLine)
		pw.propertyLabel[i].SetBackgroundColor(ui.Color_LightGreen)

		pw.propertyValue[i] = ui.GoLineEdit(pw.propertyValueLayout)
		pw.propertyValue[i].SetSize(200, 20)
		pw.propertyValue[i].SetMargin(0,0,0,0)
		pw.propertyValue[i].SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
		//pw.value[i].SetBorderStyle(ui.BorderSingleLine)
		pw.propertyValue[i].SetBackgroundColor(ui.Color_LightGreen)

		pw.property[pw.widgetProperties[i]] = i
	}
}

func (pw *GoPropertiesObj) SetObjectProperties(obj GoObject) {
	switch obj.objectType() {
		case "GoFormObj":
			ctrl := obj.(*GoFormObj)
			log.Println("ObjectType", ctrl.objectType())
		case "GoLayoutObj":
			ctrl := obj.(*GoLayoutObj)
			log.Println("ObjectType", ctrl.objectType())
		case "GoButtonObj":
			ctrl := obj.(*GoButtonObj)
			log.Println("ObjectType", ctrl.objectType())
		//case "GoLabelObj":
			//ctrl := *GoLabelObj(obj)
		//case "GoLineEditObj":
			//ctrl := *GoLineEditObj(obj)
		case "GoCheckBoxObj":
			ctrl := obj.(*GoCheckBoxObj)
			log.Println("ObjectType", ctrl.objectType())
		//case "GoPanelObj":
			//ctrl := *GoPanelObj(obj)
		case "GoRadioButtonObj":
			ctrl := obj.(*GoRadioButtonObj)
			log.Println("ObjectType", ctrl.objectType())
		//case "GoTextEditObj":
			//ctrl := *GoTextEditObj(obj)
		//case "GoViewObj":
			//ctrl := *GoViewObj(obj)
		
	}
	//parent := ctrl.parentWidget

	if wid := obj.wid(); wid != nil {
		pw.propertyLabel[pw.property["X"]].Show()
		pw.propertyLabel[pw.property["Y"]].Show()
		pw.propertyLabel[pw.property["Width"]].Show()
		pw.propertyLabel[pw.property["Height"]].Show()
		pw.propertyLabel[pw.property["OffsetX"]].Show()
		pw.propertyLabel[pw.property["OffsetY"]].Show()
		pw.propertyLabel[pw.property["ClientX"]].Show()
		pw.propertyLabel[pw.property["ClientY"]].Show()
		pw.propertyLabel[pw.property["ClientWidth"]].Show()
		pw.propertyLabel[pw.property["ClientHeight"]].Show()
		pw.propertyLabel[pw.property["MarginLeft"]].Show()
		pw.propertyLabel[pw.property["MarginTop"]].Show()
		pw.propertyLabel[pw.property["MarginRight"]].Show()
		pw.propertyLabel[pw.property["MarginBottom"]].Show()
		pw.propertyLabel[pw.property["PaddingLeft"]].Show()
		pw.propertyLabel[pw.property["PaddingTop"]].Show()
		pw.propertyLabel[pw.property["PaddingRight"]].Show()
		pw.propertyLabel[pw.property["PaddingBottom"]].Show()


		pw.propertyValue[pw.property["X"]].SetText(str.Itoa(wid.x))
		pw.propertyValue[pw.property["X"]].Show()
		pw.propertyValue[pw.property["Y"]].SetText(str.Itoa(wid.y))
		pw.propertyValue[pw.property["Y"]].Show()
		pw.propertyValue[pw.property["Width"]].SetText(str.Itoa(wid.width))
		pw.propertyValue[pw.property["Width"]].Show()
		pw.propertyValue[pw.property["Height"]].SetText(str.Itoa(wid.height))
		pw.propertyValue[pw.property["Height"]].Show()

		pw.propertyValue[pw.property["OffsetX"]].SetText(str.Itoa(wid.clientOffsetX))
		pw.propertyValue[pw.property["OffsetX"]].Show()
		pw.propertyValue[pw.property["OffsetY"]].SetText(str.Itoa(wid.clientOffsetY))
		pw.propertyValue[pw.property["OffsetY"]].Show()

		pw.propertyValue[pw.property["ClientX"]].SetText(str.Itoa(wid.clientX))
		pw.propertyValue[pw.property["ClientX"]].Show()
		pw.propertyValue[pw.property["ClientY"]].SetText(str.Itoa(wid.clientY))
		pw.propertyValue[pw.property["ClientY"]].Show()
		pw.propertyValue[pw.property["ClientWidth"]].SetText(str.Itoa(wid.clientWidth))
		pw.propertyValue[pw.property["ClientWidth"]].Show()
		pw.propertyValue[pw.property["ClientHeight"]].SetText(str.Itoa(wid.clientHeight))
		pw.propertyValue[pw.property["ClientHeight"]].Show()

		pw.propertyValue[pw.property["MarginLeft"]].SetText(str.Itoa(wid.margin.Left))
		pw.propertyValue[pw.property["MarginLeft"]].Show()
		pw.propertyValue[pw.property["MarginTop"]].SetText(str.Itoa(wid.margin.Top))
		pw.propertyValue[pw.property["MarginTop"]].Show()
		pw.propertyValue[pw.property["MarginRight"]].SetText(str.Itoa(wid.margin.Right))
		pw.propertyValue[pw.property["MarginRight"]].Show()
		pw.propertyValue[pw.property["MarginBottom"]].SetText(str.Itoa(wid.margin.Bottom))
		pw.propertyValue[pw.property["MarginBottom"]].Show()

		pw.propertyValue[pw.property["PaddingLeft"]].SetText(str.Itoa(wid.padding.Left))
		pw.propertyValue[pw.property["PaddingLeft"]].Show()
		pw.propertyValue[pw.property["PaddingTop"]].SetText(str.Itoa(wid.padding.Top))
		pw.propertyValue[pw.property["PaddingTop"]].Show()
		pw.propertyValue[pw.property["PaddingRight"]].SetText(str.Itoa(wid.padding.Right))
		pw.propertyValue[pw.property["PaddingRight"]].Show()
		pw.propertyValue[pw.property["PaddingBottom"]].SetText(str.Itoa(wid.padding.Bottom))
		pw.propertyValue[pw.property["PaddingBottom"]].Show()

	}
}


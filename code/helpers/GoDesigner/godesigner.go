/* godesigner.go */

package main

import (
	"log"
	_ "os"
	_ "path/filepath"

	ui "gitlab.com/win32go/win32"

)

var app *ui.GoWinApplication
//var desktop *ui.GoDeskTopWindow
var previewWindow *ui.GoWindowObj
var preview *ui.GoLayoutObj
var propertiesWindow *GoPropertiesObj
//var propertyNameLayout *ui.GoLayoutObj
//var propertyValueLayout *ui.GoLayoutObj
var controlWindow *ui.GoWindowObj
var form *GoFormObj
var template *GoTemplateObj
var controlButtonList [4]*ui.GoLabelObj 	// number of different control selections


var dragNode int
	const (
		dragNone 	= 0
		dragXLeft	= 1
		dragXRight	= 2
		dragYUp    	= 3
		drageYDown	= 4
		dragXYLeftUp   	= 5
		dragXYLeftDown	= 6
		dragXYRightUp	= 7
		dragXYRightDown	= 8
	)
var mouseAction int
	const (
		idleControl = 0
		addingControl = 1
		movingControl = 2
		sizingControl = 3
	)
	
var dragStartX, dragStartY, dragStartWidth, dragStartHeight int

var dragControl GoObject
//var dragTemplate *goTemplate
var inProcess bool

func main() {
	app = ui.GoApplication("GoApplication")

	//var highlightedTemplate, controlToAdd ui.GoObject
	//var templateDx, templateDy int

	// Main Window providing the Layout Window

	previewWindow = ui.GoWindow(nil)
	previewWindow.SetTitle("GoDesigner - Preview Window")
	previewWindow.SetPosition(400, 30)
	previewWindow.SetSize(800, 600)
	previewWindow.SetPadding(40, 40, 40, 40)
	previewWindow.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	previewWindow.SetOnMouseDown(Preview_MouseDown)
	previewWindow.SetOnMouseMove(Preview_MouseMove)
	previewWindow.SetOnMouseUp(Preview_MouseUp)
	previewWindow.SetBackgroundColor(ui.Color_AliceBlue)
	//previewWindow.SetOnPaint(PreviewWindow_Paint)
	previewWindow.Show()

	// Layout Window containig UI Form

	preview = previewWindow.Layout()
	preview.SetBorderStyle(ui.BorderSingleLine)
	//preview.SetOnMouseDown(Preview_MouseDown)
	//preview.SetOnMouseMove(Preview_MouseMove)
	//preview.SetOnMouseUp(Preview_MouseUp)
	preview.SetOnPaint(Preview_Paint)
	preview.SetOnResize(Preview_Resize)
	
	// Property Editor displaying selected control's properties

	propertiesWindow = GoPropertiesWindow(previewWindow)
	propertiesWindow.Show()

	// ToolBar displaying available control widgets

	controlWindow = ui.GoWindow(previewWindow)
	controlWindow.SetTitle("GoDesigner")
	controlWindow.SetPosition(150, 30)
	controlWindow.SetSize(250, 600)
	controlWindow.SetLayoutStyle(ui.VBoxLayout)
	controlWindow.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	controlWindow.SetOnMouseDown(Control_MouseDown)
	controlWindow.SetOnMouseMove(Control_MouseMove)
	controlWindow.SetOnMouseUp(Control_MouseUp)
	controlWindow.SetBackgroundColor(ui.Color_AliceBlue)
	controlWindow.Show()
	ControlWindow_CreateMenu()

	// Controls available in ToolBar

	controlLayout := controlWindow.Layout()
	controlLayout.SetSpacing(0)

	controlCaption := ui.GoLabel(controlLayout, " Control Selector")
	controlCaption.SetSize(200, 24)
	controlCaption.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//propertiesCaption.SetBorderStyle(ui.BorderSingleLine)
	controlCaption.SetTextColor(ui.Color_White)
	controlCaption.SetBackgroundColor(ui.Color_LightGray)
	controlCaption.Show()

	buttonCaption := ui.GoLabel(controlLayout, "Buttons")
	buttonCaption.SetSize(200, 20)
	buttonCaption.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//buttonCaption.SetBorderStyle(ui.BorderSingleLine)
	buttonCaption.SetAlignment(ui.AlignHCenter)
	buttonCaption.SetTextColor(ui.Color_White)
	buttonCaption.SetBackgroundColor(ui.Color_LightGray)
	buttonCaption.Show()
	
	
	layoutTemplate := ui.GoLabel(controlLayout, " + Layout")
	layoutTemplate.SetSize(100, 17)
	layoutTemplate.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	layoutTemplate.SetOnClick(ActionNewTemplate_Clicked)
	layoutTemplate.Show()
	controlButtonList[0] = layoutTemplate

	buttonTemplate := ui.GoLabel(controlLayout, " + Button")
	buttonTemplate.SetSize(100, 17)
	buttonTemplate.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	buttonTemplate.SetOnClick(ActionNewTemplate_Clicked)
	buttonTemplate.Show()
	controlButtonList[1] = buttonTemplate

	checkBoxTemplate := ui.GoLabel(controlLayout, " + CheckBox")
	checkBoxTemplate.SetSize(100, 17)
	checkBoxTemplate.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	checkBoxTemplate.SetOnClick(ActionNewTemplate_Clicked)
	checkBoxTemplate.Show()
	controlButtonList[2] = checkBoxTemplate

	radioButtonTemplate := ui.GoLabel(controlLayout, " + RadioButton")
	radioButtonTemplate.SetSize(100, 17)
	radioButtonTemplate.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	radioButtonTemplate.SetOnClick(ActionNewTemplate_Clicked)
	radioButtonTemplate.Show()
	controlButtonList[3] = radioButtonTemplate

	// The new UI Form

	form = GoForm(preview)
	propertiesWindow.SetObjectProperties(form)
	// * IMPORTANT * to calculate initial sizes of titlebar caption and buttons
	preview.Resize()
	
	app.Run()
}

func ControlWindow_CreateMenu() {
	controlMenu := controlWindow.AddMenuBar()
	
	//_____________
	menuFile := controlMenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuEdit := controlMenu.AddMenu("&Edit")
	//+++++++++++++
	/*actionDelete := */menuEdit.AddAction("&Delete")
	/*actionCut := */menuEdit.AddAction("&Cut")
	/*actionCopy := */menuEdit.AddAction("&Copy")
	/*actionPaste := */menuEdit.AddAction("&Paste")
	//_____________
	menuView := controlMenu.AddMenu("&View")
	//+++++++++++++
	/*actionViewCode := */menuView.AddAction("&View Code...")
	//_____________
	menuHelp := controlMenu.AddMenu("&Help")
	//+++++++++++++
	actionHelp := menuHelp.AddAction("&Help")
	actionHelp.SetOnActivate(ActionHelp_Clicked)
	actionAbout := menuHelp.AddAction("&About")
	actionAbout.SetOnActivate(ActionAbout_Clicked)
}

func ActionAbout_Clicked() {

}

func ActionExit_Clicked() {
	//exitdialog := ui.GoMessageBox(mainWindow, ui.Question, "Closing GoDesigner.", "Do you really want to exit?", 7, 6)
	//exitdialog.SetTitle("GoDesigner")
	//exitdialog.SetBackgroundColor(ui.Color_AliceBlue)
	//exitdialog.CaptionBox().SetBackgroundColor(ui.Color_Gray)
	//exitdialog.CaptionBox().SetPadding(5,5,5,5)
	//exitdialog.CaptionBox().SetBorderStyle(ui.BorderRaised)
	//exitdialog.SetCaptionColor(ui.Color_Blue)
	//exitdialog.SetMessageColor(ui.Color_Black)
	//exitdialog.MessageBox().SetBorderStyle(ui.BorderSunken)

	//button := exitdialog.Exec()
	//log.Println("button =", button)
	//if button == 6 {
		ui.GoApp().Exit()
	//}
	
}

func ActionHelp_Clicked() {

}

func ActionNewTemplate_Clicked(label *ui.GoLabelObj) {
	log.Println("..........ActionNewButton_Clicked")
	
	if form != nil {

		if template != nil {
			Control_BlitTemplate()
			mouseAction = idleControl
			template = nil
		}
			
		/*for _, l := range controlButtonList {
			if label == l {
				log.Println("Set Label:", l.Text(), ":Color_LightGrey")
				l.SetBackgroundColor(ui.Color_LightGray)
			} else {
				log.Println("Set Label:", l.Text(), ":Color_AliceBlue")
				l.SetBackgroundColor(ui.Color_AliceBlue)
			}
		}*/
		//controlWindow.Refresh()
		switch label.Text() {
			case " + Layout":
				mouseAction = addingControl
				template = GoTemplate(LayoutTemplate)
			case " + Button":
				mouseAction = addingControl
				template = GoTemplate(ButtonTemplate)
			case " + RadioButton":
				mouseAction = addingControl
				template = GoTemplate(RadioButtonTemplate)
			case " + CheckBox":
				mouseAction = addingControl
				template = GoTemplate(CheckBoxTemplate)
			default:
		}
		Control_RefreshButtonList()
	}
}

func Control_RefreshButtonList() {
	if template == nil {
		for _, l := range controlButtonList {
			l.SetBackgroundColor(ui.Color_AliceBlue)
		}
	} else {
		for idx, l := range controlButtonList {
			if int(template.templateType) == idx {
				log.Println("Set Label:", l.Text(), ":Color_LightGrey")
				l.SetBackgroundColor(ui.Color_LightGray)
			} else {
				log.Println("Set Label:", l.Text(), ":Color_AliceBlue")
				l.SetBackgroundColor(ui.Color_AliceBlue)
			}
			
		}
	}
}

func Control_MouseDown(button ui.GoButtonState, x, y int) {
	
	log.Println("..........Control Window")
		if button == ui.LeftButton {
			log.Println("..........LeftButton DOWN - x:", x, " y:", y)
			if template != nil {
				Control_BlitTemplate()
				template = nil
				Control_RefreshButtonList()
			}
		} else if button == ui.MiddleButton {

		} else if button == ui.RightButton {
			log.Println("..........RightButton DOWN - x:", x, " y:", y)
		}
	
}

func Control_MouseMove(x, y int) {
	//log.Println("..........Control Window MouseMove")
	if template != nil {
		if template.focus == "preview" {
			Preview_BlitTemplate()
			template.focus = "control"
		} else if template.focus == "" {
			template.focus = "control"
		} else {
			Control_BlitTemplate()
		}
		template.moveTo(x, y)
		Control_BlitTemplate()
	}
}

func Control_MouseUp(button ui.GoButtonState, x, y int) {
	
	log.Println("..........Control Window")
		if button == ui.LeftButton {
			log.Println("..........LeftButton UP - x:", x, " y:", y)

		} else if button == ui.MiddleButton {

		} else if button == ui.RightButton {
			log.Println("..........RightButton UP - x:", x, " y:", y)
		}
	
}

func Control_BlitTemplate() {
	if template != nil {
		hDC := controlWindow.GetDC()
		rect := controlWindow.GetWindowRect()
		pt := ui.CreatePainter(hDC, rect)
		template.blit(pt)
		pt.Destroy()
		controlWindow.ReleaseDC(hDC)
	}
}

func Preview_MouseDown(button ui.GoButtonState, x, y int) {
	log.Println("..........Preview Window")
	x -= 40
	y -= 40
	if (x > 0) && (x < previewWindow.Width() - 80) && (y > 0) && (y < previewWindow.Height() - 80) {
		if button == ui.LeftButton {
			log.Println("..........LeftButton DOWN - x:", x, " y:", y)
			if form != nil {
				if template != nil {
					Preview_BlitTemplate()
					form.MouseDown(button, x, y)
					template = nil
					Control_RefreshButtonList()
				} else {
					form.MouseDown(button, x, y)
				}
			}
		} else if button == ui.MiddleButton {

		} else if button == ui.RightButton {
			log.Println("..........RightButton DOWN - x:", x, " y:", y)
		}
	}
}

func Preview_MouseMove(x int, y int) {
	//log.Println("..........Preview Window MouseMove")
	
	x -= 40
	y -= 40
	if template != nil {
		if template.focus == "control" {
			Control_BlitTemplate()
			template.focus = "preview"
		} else {
			Preview_BlitTemplate()
		}
		template.moveTo(x, y)
		Preview_BlitTemplate()
	} else {
		if form != nil {
			if (x > 0) && (x < previewWindow.Width() - 80) && (y > 0) && (y < previewWindow.Height() - 80) {
			//log.Println("..........Preview Window MouseMove")
				form.MouseMove(x, y)
			}
		}
	}
	
}

func Preview_MouseUp(button ui.GoButtonState, x, y int) {
	log.Println("..........Preview Window")
	x -= 40
	y -= 40
	if x > 0 && x < previewWindow.Width() - 80 && y > 0 && y < previewWindow.Height() - 80 {
		if button == ui.LeftButton {
			log.Println("..........LeftButton UP - x:", x, " y:", y)
			if form != nil {
				form.MouseUp(button, x, y)
			}
		} else if button == ui.MiddleButton {

		} else if button == ui.RightButton {
			log.Println("..........RightButton UP - x:", x, " y:", y)
		}
	}
}

func Preview_BlitTemplate() {
	hDC := preview.GetDC()
	rect := preview.GetWindowRect()
	pt := ui.CreatePainter(hDC, rect)
	template.blit(pt)
	pt.Destroy()
	preview.ReleaseDC(hDC)
}

func Preview_Paint(painter *ui.GoPainter) {
	log.Println("..........Preview_Paint")
	rc := preview.GetClientRect()
	log.Println("PreviewLayoutRect(", rc.Left(), rc.Top(), rc.Width(), rc.Height(), " )")
	if form != nil {
		form.Paint(painter)
	}
}

func Preview_Resize(clientRect *ui.GoRectType) {
	if form != nil {
		log.Println("..........Preview_Resize")
		form.Resize(clientRect)
	}
}

func PreviewWindow_Paint(painter *ui.GoPainter) {
	log.Println("PreviewWindowPaint()..................")
	rc := previewWindow.GetClientRect()
	log.Println("PreviewWindowRect(", rc.Left(), rc.Top(), rc.Width(), rc.Height(), " )")
}


/* goapplication.go */

package main

import (
	"log"
	"fmt"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var desktop *ui.GoDeskTopWindow
var mainwindow1 *ui.GoWindowObj
var mainwindow2 *ui.GoWindowObj
var popup1 *ui.GoWindowObj
//var ccheckbox1 *ui.GoCheckBoxObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app := ui.GoApplication(appName)

	desktop = app.GoDeskTop()
	//screenCX := desktop.ClientHeight()
	//screenCY := desktop.ClientWidth()

	mainwindow1 = ui.GoWindow(nil)
	mainwindow1.SetTitle("GoWindows Demo #1")
	mainwindow1.SetMargin(40, 40, 30, 40)
	mainwindow1.SetOnPaint(MainWindow1_Paint)
	//mainwindow1.PositionOnScreen()
	mainwindow1.Show()



	mainwindow2 = ui.GoWindow(nil)
	mainwindow2.SetTitle("GoWindows Demo #2")
	mainwindow2.SetMargin(40, 40, 40, 40)
	mainwindow2.SetOnPaint(MainWindow2_Paint)
	mainwindow2.Show()
		
	mainmenu := mainwindow1.AddMenuBar()

	menuApp := mainmenu.AddMenu("&Application")

	actionPopup := menuApp.AddAction("&Popup")
	actionPopup.SetOnActivate(ActionPopup_Clicked)
	actionExit := menuApp.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)



	ret := app.Run()
	fmt.Println("Window Closed return =", ret)
}

func MainWindow1_Paint(p *ui.GoPainter) {
	log.Println("MainWindow1_Paint..............")
	blue := ui.ColorFromRGB(0, 0, 255)
	log.Println("CreatePen(blue):")	
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X()+10, p.Y()+10, p.Width()-20, p.Height()-20)

}

func MainWindow2_Paint(p *ui.GoPainter) {
	log.Println("MainWindow2_Paint..............")
	red := ui.ColorFromRGB(255, 0, 0)
	log.Println("SetStockPenColor(red):")
	p.SetStockPen(red)
	p.DrawRect(p.X()+10, p.Y()+10, p.Width() - 20, p.Height() - 20)
}

func Popup1_Paint(p *ui.GoPainter) {
	log.Println("Popup_Paint..............")
	red := ui.ColorFromRGB(255, 0, 0)
	log.Println("SetStockPenColor(red):")
	p.SetStockPen(red)
	p.DrawRect(p.X() + 10, p.Y() + 10, p.Width() - 20, p.Height() - 20)
}

func ActionPopup_Clicked() {
	popup1 = ui.GoPopupWindow(mainwindow1)
	popup1.SetTitle("Popup #1")
	popup1.SetMargin(20, 20, 20, 20)
	popup1.SetOnPaint(Popup1_Paint)
	//popup.PositionOnScreen()
	popup1.Show()
}

func ActionExit_Clicked() {
	ui.GoApp().Exit()	
}
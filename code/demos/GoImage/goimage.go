/* win32go/code/demos/GoLineEdit/goimage.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj

//var panel1 *ui.GoPanelObj
var view1 *ui.GoViewObj
var exitdialog *ui.GoMessageBoxObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoImage Demo")
	mainwindow.SetHeight(700)
	mainwindow.SetWidth(800)
	mainwindow.SetLayoutStyle(ui.HBoxLayout)
	//mainwindow.SetLayoutSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	mainwindow.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	
	
	
	
	//mainwindow.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//mainwindow.SetLayoutPadding(40, 40, 40, 40)
	//mainwindow.SetBackgroundColor(ui.Color_AliceBlue)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.SetOnCanClose(MainWindow_CanClose)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuView := mainmenu.AddMenu("&View")
	//+++++++++++++
	actionImage := menuView.AddAction("&NewImage")
	actionImage.SetOnActivate(ActionImage_Clicked)
	//_____________
	menuMessage := mainmenu.AddMenu("&Message")
	//+++++++++++++
	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	//panel1 = ui.GoPanel(mainwindow)
	//panel1.SetBackgroundColor(ui.Color_AliceBlue)
	//panel1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//panel1.SetMargin(40, 40, 40, 40)
	view1 = ui.GoView(mainwindow)
	view1.SetBackgroundColor(ui.Color_AliceBlue)
	view1.SetLayoutStyle(ui.HBoxLayout)
	view1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//view1.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	//view1.SetMargin(40, 40, 40, 40)
	//view1.SetPosition(0, 0)
	//view1.SetWidth(500)
	//view1.SetHeight(500)
	
	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_CanClose() bool {
	exitdialog = ui.GoMessageBox(mainwindow, ui.Question, "Application Shutdown.", "Do you really want to exit?", 7, 6)
	exitdialog.SetTitle("GoMessageBox Demo")
	exitdialog.SetCaptionColor(ui.Color_Blue)
	exitdialog.SetMessageColor(ui.Color_Black)
	ret := exitdialog.Exec()
	log.Println("ret =", ret)
	if ret == ui.IDYES {
		return true
	} else { 
		return false
	}
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 10, p.Y() + 10, p.Width() - 20, p.Height() - 20)
	
}
func ActionExit_Clicked() {
	ok := MainWindow_CanClose()
	if ok {
		ui.GoApp().Exit()
	}
}

func ActionImage_Clicked() {
	testImage := ui.GoImage("C:\\godev\\src\\gitlab.com\\win32go\\project\\demos\\testImage.jpg")
	if testImage != nil {
		view1.AddImage(testImage)
	}
	view1.Show()
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!\n")
}



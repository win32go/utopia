/* win32go/code/demos/GoMenus/golayout.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var buttonExit *ui.GoButtonObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	if filepath.Ext(appName) == ".exe" {
		appName = appName[:len(appName) - 4]	// GoApplication
	}

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetLayoutStyle(ui.HBoxLayout)
	mainwindow.SetLayoutSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	mainwindow.SetTitle("Golayout Demo")
	mainwindow.SetBackgroundColor(ui.Color_AliceBlue)
	mainwindow.SetOnPaint(MainWindow_Paint)
	
	//mainwindow.SetPadding(16, 16, 16, 16)
	mainwindow.SetLayoutHeight(700)
	mainwindow.SetLayoutWidth(800)


	mainwindow.Show()

	log.Println("SETTiNG mainlayout ******************")
	mainlayout := mainwindow.Layout()
	//mainlayout.SetBorderStyle(ui.BorderSingleLine)
	//mainlayout.SetBackgroundColor(ui.Color_Silver)
	//mainlayout.SetStyle(ui.VBoxLayout)
	mainlayout.SetPadding(40, 40, 40, 40)
	//mainlayout.SetBorderStyle(ui.BorderSingleLine)
	//mainlayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//mainlayout.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	/*log.Println("mainlayout.x = ", mainlayout.X())
	log.Println("mainlayout.y = ", mainlayout.Y())
	log.Println("mainlayout.width = ", mainlayout.Width())
	log.Println("mainlayout.height = ", mainlayout.Height())*/


	log.Println("CREATING layout1 ******************")
	layout1 := ui.GoLayout(mainlayout, ui.HBoxLayout)
	layout1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//layout1.SetHeight(420)
	//layout1.SetBorderStyle(ui.BorderSingleLine)
	layout1.SetBackgroundColor(ui.Color_AliceBlue)
	layout1.SetPadding(4, 4, 4, 4)
	layout1.SetSpacing(0)
	layout1.Show()

	log.Println("CREATING layout2 ******************")
	layout2 := ui.GoLayout(layout1, ui.HBoxLayout)
	layout2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//layout2.SetHeight(420)
	//layout2.SetBorderStyle(ui.BorderSingleLine)
	layout2.SetBackgroundColor(ui.Color_Orange)
	layout2.Show()

	/*log.Println("CREATING layout3 ******************")
	layout3 := ui.GoLayout(layout1, ui.HBoxLayout)
	layout3.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//layout2.SetHeight(420)
	//layout3.SetBorderStyle(ui.BorderSingleLine)
	layout3.SetBackgroundColor(ui.Color_Orange)
	layout3.Show()*/

	mainmenu := mainwindow.AddMenuBar()
	  menuFile := mainmenu.AddMenu("&File")
	    /*actionNew := */menuFile.AddAction("&New")
	    /*actionOpen := */menuFile.AddAction("&Open")
	    /*actionSave := */menuFile.AddAction("&Save")
	    /*actionSaveAs := */menuFile.AddAction("Save&As")
	    menuFile.AddSeparator()
	    actionExit := menuFile.AddAction("&Exit")
	    actionExit.SetOnActivate(ActionExit_Clicked)
	  /*menuMessage := mainmenu.AddMenu("&Message")
	    actionMessage := menuMessage.AddAction("&Message")
	    actionMessage.SetOnActivate(ActionMessage_Clicked)*/

	
	/*labelHello := ui.GoLabel(layout1, "Hello Richard")
	//log.Println("labelHello.SetSize")
	labelHello.SetSize(140, 30)
	labelHello.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelHello.Show()*/

	/*labelExit := ui.GoLabel(layout1, "Click Button")
	//log.Println("labelExit.SetSize")
	labelExit.SetSize(140, 30)
	labelExit.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelExit.Show()*/

	/*buttonExit = ui.GoButton(mainwindow, "Exit")
	//log.Println("buttonExit.SetSize")
	buttonExit.SetSize(100, 30)
	buttonExit.SetOnClick(ActionExit_Clicked)
	//log.Println("buttonExit.Show()")
	buttonExit.Show()*/

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.SetNullBrush()
	p.DrawRect(10, 10, 800 - 20, 700 - 20)
	
}
func ActionExit_Clicked() {
	ui.GoApp().Exit()	
}

func ActionMessage_Clicked() {
	
}

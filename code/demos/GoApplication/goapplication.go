/* goapplication.go */

package main

import (
	ui "gitlab.com/win32go/win32"
)

func main() {
	app := ui.GoApplication("GoApplication")
	win := ui.GoWindow(nil)
	win.SetTitle("GoApplication Demo")
	win.Show()

	app.Run()
}

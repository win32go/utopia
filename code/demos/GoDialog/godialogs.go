/* win32go/code/demos/GoMenus/godialogs.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var exitdialog *ui.GoDialogObj


func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoDialog Demo")
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	exitdialog = ui.GoDialog(mainwindow)
	exitdialog.SetCaption("Shutdown Now?")
	//exitlayout := exitdialog.Layout()
	//exitlayout.SetStyle(ui.VBoxLayout)
	//exitlayout.SetPadding(40,40,40,40)
	//dialog.Show()
	
	mainmenu := mainwindow.AddMenuBar()

	menuFile := mainmenu.AddMenu("&File")
	
	/*actionNew :=*/ menuFile.AddAction("&New")
	/*actionOpen :=*/ menuFile.AddAction("&Open")
	/*actionSave :=*/ menuFile.AddAction("&Save")
	/*actionSaveAs :=*/ menuFile.AddAction("Save&As")
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)

	menuMessage := mainmenu.AddMenu("&Message")

	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 10, p.Y() + 10, p.Width() - 20, p.Height() - 20)
	
}
func ActionExit_Clicked() {
	log.Println("ActionExit_Clicked: waiting")
	ret :=  exitdialog.Exec()
	log.Println("ExitDialog returns:", ret)
	if ret == ui.IDOK {
		ui.GoApp().Exit()
	}	
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!")
}

/* godesktop.go */

package main

import (
	"log"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var desktop *ui.GoDeskTopWindow
var mainwindow *ui.GoWindowObj
var buttonExit *ui.GoButtonObj

func main() {
	var screenMetrics string

	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	if filepath.Ext(appName) == ".exe" {
		appName = appName[:len(appName) - 4]	// GoApplication
	}

	app = ui.GoApplication(appName)

	desktop = app.GoDeskTop()
	
	screenMetrics = "Screen Height ...... " + strconv.Itoa(desktop.Height()) + " pixels\r\n"
	screenMetrics += "Screen Width ....... " + strconv.Itoa(desktop.Width()) + " pixels\r\n"

	screenMetrics += "Screen ClientHeight ...... " + strconv.Itoa(desktop.ClientHeight()) + " pixels\r\n"
	screenMetrics += "Screen ClientWidth ....... " + strconv.Itoa(desktop.ClientWidth()) + " pixels\r\n"

	screenMetrics += "Screen Vertical Size ...... " + strconv.Itoa(desktop.VerticalSize()) + " mm\r\n"
	screenMetrics += "Screen Horizontal Size .... " + strconv.Itoa(desktop.HorizontalSize()) + " mm\r\n"

	screenMetrics += "Screen Vertical Res ...... " + strconv.Itoa(desktop.VerticalRes()) + " pixels/inch\r\n"
	screenMetrics += "Screen Horizontal Res .... " + strconv.Itoa(desktop.HorizontalRes()) + " pixels/inch\r\n"

	

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle(appName + " Demo")
	mainwindow.SetLayoutStyle(ui.VBoxLayout)
	mainwindow.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	mainwindow.SetMargin(40, 40, 40, 40)
	mainwindow.SetLayoutPadding(10, 10, 10, 10)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	centerlayout := mainwindow.AddLayout(ui.VBoxLayout)
	centerlayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	centerlayout.SetPadding(10, 10, 10, 10)
	centerlayout.Show()

	controlbar := mainwindow.AddLayout(ui.HBoxLayout)
	controlbar.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	controlbar.SetHeight(40)
	controlbar.MarginSetLeft(100)
	controlbar.Show()

	textbox := ui.GoTextEdit(centerlayout)
	textbox.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	textbox.SetText(screenMetrics)
	textbox.Show()
	//mainwindow.TextOut(screenMetrics)
	
	buttonExit = ui.GoButton(controlbar, "Exit")
	buttonExit.SetPosition(280, 0)
	buttonExit.SetOnClick(ActionExit_Clicked)
	buttonExit.Show()

	ret := app.Run()
	fmt.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	log.Println("ActionMainWindow_Paint.............")
	// create colors
	blue := ui.ColorFromRGB(0, 0, 255)
	red := ui.ColorFromRGB(255, 0, 0)
	// select stock pen color red
	p.SetStockPen(blue)
	// draw rectangle border 5px outside margin
	p.DrawRect(p.X() + 5, p.Y() + 5, p.Width() - 10, p.Height() - 10)
	// create pen blue color, 3pt wide, solidline, reference blue5
	pen := ui.CreatePen(red, 3, 1, "red5")
	// select into painter, new pen 
	p.SetPen(pen)
	// draw rectangle border frame 10px outside margin
	p.DrawRect(p.X()+10, p.Y()+10, p.Width()-20, p.Height()-20)

}

func ActionExit_Clicked() {
	log.Println("ActionExit_Clicked: waiting")
	ui.GoApp().Exit()	
}
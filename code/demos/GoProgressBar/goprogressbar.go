/* win32go/code/demos/GoProgressBar/goprogressbar.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var progressbar1 *ui.GoProgressBarObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoProgressBar Demo")
	mainwindow.SetMargin(40, 40, 40, 40)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuProgressBar := mainmenu.AddMenu("&ProgressBar")
	//+++++++++++++
	actionProgressBar := menuProgressBar.AddAction("&New")
	actionProgressBar.SetOnActivate(ActionProgressBar_Clicked)
	//_____________
	menuMessage := mainmenu.AddMenu("&Message")
	//+++++++++++++
	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	progressbar1 = ui.GoProgressBar(mainwindow, 100)	// ui.PB_Marquee
	progressbar1.SetTotalSteps(1000)
	progressbar1.SetPosition(100, 200)
	progressbar1.SetHeight(28)
	progressbar1.SetWidth(200)
	progressbar1.Show()
	
	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() - 10, p.Y() - 10, p.Width() + 20, p.Height() + 20)
	
}
func ActionExit_Clicked() {
	//messageBox := ui.GoMessageBox(mainwindow, ui.Question, "Application Shutdown.", "Do you really want to exit?")
	//button := messageBox.Exec()
	//log.Println("button =", button)
	//if button == 6 {
		ui.GoApp().Exit()
	//}
	
}

func ActionProgressBar_Clicked() {
	progressbar1.SetProgress(500)
	//progressbar1.ShowMarquee()
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!\n")
}

/* win32go/code/demos/GoMenus/gomenus.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj


func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoMenus Demo")
	mainwindow.SetMargin(40, 40, 40, 40)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()

	menuFile := mainmenu.AddMenu("&File")
	
	newIcon, _ := ui.GoIconFromImage(app.WorkingDirectory() + "\\GoMenu.ico")
	/*actionNew :=*/ menuFile.AddAction("&New", newIcon)
	/*actionOpen :=*/ menuFile.AddAction("&Open")
	/*actionSave :=*/ menuFile.AddAction("&Save")

	/*actionSaveAs :=*/ menuFile.AddAction("Save&As")
	menuFile.AddSeparator()
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	
	menuMessage := mainmenu.AddMenu("&Message")

	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	buttonExit := ui.GoButton(mainwindow, "Exit")
	buttonExit.SetPosition(280, 200)
	buttonExit.SetOnClick(ActionExit_Clicked)
	buttonExit.Show()

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() - 10, p.Y() - 10, p.Width() + 20, p.Height() + 20)
	
}
func ActionExit_Clicked() {
	ui.GoApp().Exit()	
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!")
}

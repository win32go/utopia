/* win32go/code/demos/GoMenus/gomessagebox.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var checkbox1 *ui.GoCheckBoxObj
var checkbox2 *ui.GoCheckBoxObj
var checkbox3 *ui.GoCheckBoxObj
var checkbox4 *ui.GoCheckBoxObj
var radiobutton1 *ui.GoRadioButtonObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoMenus Demo")
	//mainwindow.SetLayoutSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	mainwindow.SetPadding(40, 40, 40, 40)
	//mainwindow.SetBackgroundColor(ui.Color_Orange)
	//mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Layout().SetBorderStyle(ui.BorderSingleLine)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuCheckBox := mainmenu.AddMenu("&CheckBox")
	//+++++++++++++
	menuNewCheckBox := menuCheckBox.AddMenu("&New")
	//++++++++++++++++++++
	actionCheckBox := menuNewCheckBox.AddAction("CheckBox")
	actionCheckBox.SetOnActivate(ActionCheckBox_Clicked)
	actionRadioButton := menuNewCheckBox.AddAction("Radio Button")
	actionRadioButton.SetOnActivate(ActionRadioButton_Clicked)
	//_____________
	menuMessage := mainmenu.AddMenu("&Message")
	//+++++++++++++
	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	checkbox1 = ui.GoCheckBox(mainwindow, "GoCheckBox")
	checkbox1.SetPosition(300, 100)
	log.Println("CHECKBOX::SetAutoSize(true).............................")
	checkbox1.SetAutoSize(true)
	checkbox1.SetOnChange(CheckBox1_Changed)
	
	checkbox2 = ui.GoCheckBox(mainwindow, "GoCheckBox")
	checkbox2.SetPosition(300, 130)
	checkbox2.SetBorderStyle(ui.BorderSunken)
	log.Println("CHECKBOX2::SetAutoSize(true).............................")
	checkbox2.SetAutoSize(true)
	
	checkbox3 = ui.GoCheckBox(mainwindow, "GoCheckBox")
	checkbox3.SetPosition(300, 160)
	checkbox3.SetBorderStyle(ui.BorderSunkenThick)
	log.Println("CHECKBOX2::SetAutoSize(true).............................")
	checkbox3.SetAutoSize(true)

	checkbox4 = ui.GoCheckBox(mainwindow, "GoCheckBox")
	checkbox4.SetPosition(300, 200)
	checkbox4.SetBorderStyle(ui.BorderRaised)
	log.Println("CHECKBOX2::SetAutoSize(true).............................")
	checkbox4.SetAutoSize(true)


	radiobutton1 = ui.GoRadioButton(mainwindow, "GoRadioButton")
	radiobutton1.SetPosition(300, 260)
	log.Println("RADIOBUTTON::SetAutoSize(true).................................")
	radiobutton1.SetAutoSize(true)
	radiobutton1.SetOnChange(RadioButton1_Changed)



	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 10, p.Y() + 10, p.Width() - 20, p.Height() - 20)
	
}
func ActionExit_Clicked() {
	//messageBox := ui.GoMessageBox(mainwindow, ui.Question, "Application Shutdown.", "Do you really want to exit?")
	//button := messageBox.Exec()
	//log.Println("button =", button)
	//if button == 6 {
		ui.GoApp().Exit()
	//}
	
}

func ActionCheckBox_Clicked() {
	checkbox1.Show()
	checkbox2.Show()
	checkbox3.Show()
	checkbox4.Show()
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!\n")
}

func ActionRadioButton_Clicked() {
	radiobutton1.Show()
}

func CheckBox1_Changed(checked bool) {
	if checked {
		mainwindow.TextOut("CheckBox Selected.\n")
	} else {
		mainwindow.TextOut("CheckBox Deselected.\n")
	}
}

func RadioButton1_Changed(checked bool) {
	if checked {
		mainwindow.TextOut("RadioButton Selected.\n")
	} else {
		mainwindow.TextOut("RadioButton Deselected.\n")
	}
}
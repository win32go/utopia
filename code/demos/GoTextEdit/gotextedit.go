/* win32go/code/demos/GoWidgets/gowidgets.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	"strconv"
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var button *ui.GoButtonObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	if filepath.Ext(appName) == ".exe" {
		appName = appName[:len(appName) - 4]	// GoApplication
	}
	log.Println("creating Application..........")
	app = ui.GoApplication(appName)
	//app.SetWindowColor(ui.COLOR_WHITESMOKE)
	log.Println("creating mainwindow..........")
	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoWidgets Demo")
	mainwindow.SetLayoutSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	mainwindow.SetLayoutStyle(ui.VBoxLayout)
	mainwindow.SetLayoutHeight(700)
	mainwindow.SetLayoutWidth(800)
	//mainwindow.SetLayoutMargin(0, 0, 0, 0)
	//mainwindow.SetLayoutPadding(0, 0, 0, 0)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	//mainlayout := mainwindow.Layout()
	//hdrlayout := ui.GoLayout(mainlayout, ui.HBoxLayout)
	hdrlayout := mainwindow.AddLayout(ui.HBoxLayout)
	hdrlayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	hdrlayout.SetHeight(50)

	//dspllayout := ui.GoLayout(mainlayout, ui.HBoxLayout)
	dspllayout := mainwindow.AddLayout(ui.HBoxLayout)
	dspllayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)

	layout1 := dspllayout.AddLayout(ui.VBoxLayout)
	layout1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//layout1.SetWidth(160)

	layout2 := dspllayout.AddLayout(ui.VBoxLayout)
	layout2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)

	hdrlayout.Show()
	dspllayout.Show()
	layout1.Show()
	layout2.Show()
	
	mainmenu := mainwindow.AddMenuBar()
	  menuFile := mainmenu.AddMenu("&File")
	    /*actionNew :=*/ menuFile.AddAction("&New")
	    /*actionOpen :=*/ menuFile.AddAction("&Open")
	    /*actionSave :=*/ menuFile.AddAction("&Save")
	    /*actionSaveAs :=*/ menuFile.AddAction("Save&As")
	    menuFile.AddSeparator()
	    actionExit := menuFile.AddAction("&Exit")
	    actionExit.SetOnActivate(ActionExit_Clicked)
	  menuMessage := mainmenu.AddMenu("&Message")
	    actionMessage := menuMessage.AddAction("&Message")
	    actionMessage.SetOnActivate(ActionMessage_Clicked)

	//hdrpanel := ui.GoPanel(nil)
	//hdrlayout.AddControl(hdrpanel)
	hdrpanel := ui.GoPanel(hdrlayout)
	hdrpanel.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	hdrpanel.SetBackgroundColor(ui.Color_AliceBlue)
	hdrpanel.SetLayoutSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	hdrpanel.SetLayoutStyle(ui.VBoxLayout)
	hdrpanel.SetLayoutPadding(10,5,10,5)
	hdrpanel.Show()    

	//hdrpanlayout := hdrpanel.Layout()
	hdrlabel := ui.GoLabel(hdrpanel, "Win32Go Widgets Demo ......")
	//hdrpanel.AddControl(hdrlabel)
	//hdrlabel := ui.GoLabelEx(hdrpanlayout, "Win32Go Widgets Demo ......")
	hdrlabel.SetAutoSize(true)
	//hdrlabel.SetBackgroundColor(ui.COLOR_WHITE)
	//hdrlabel.SetSize(200, 30)
	//hdrlabel.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	hdrlabel.Show()

	labelDemo1 := ui.GoLabel(layout1, "GoLabel :")
	labelDemo1.SetAutoSize(true)
	labelDemo1.MarginSetBottom(0)
	//labelDemo1.SetSize(140, 30)
	//labelDemo1.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo1.Show()

	label := ui.GoLabel(layout1, "Welcome Label")
	label.SetAutoSize(true)
	label.MarginSetLeft(40)
	label.MarginSetTop(0)
	//label.SetSize(200, 30)
	//label.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	label.Show()

	labelDemo2 := ui.GoLabel(layout1, "GoCheckBox :")
	labelDemo2.SetAutoSize(true)
	//labelDemo2.SetSize(140, 30)
	labelDemo2.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo2.Show()

	checkBox := ui.GoCheckBox(layout1, "Show Desktop Settings")
	checkBox.MarginSetLeft(40)
	checkBox.MarginSetTop(0)
	checkBox.SetAutoSize(true)
	checkBox.SetSize(200, 30)
	checkBox.SetBorderStyle(ui.BorderRaised)
	//checkBox.SetSize(200, 30)
	checkBox.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	checkBox.Show()

	labelDemo3 := ui.GoLabel(layout1, "GoRadioButton :")
	labelDemo3.SetAutoSize(true)
	//labelDemo3.SetSize(140, 30)
	labelDemo3.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo3.Show()

	radioButton := ui.GoRadioButton(layout1, "Show Desktop Settings")
	radioButton.MarginSetLeft(40)
	radioButton.MarginSetTop(0)
	radioButton.SetSize(200, 30)
	radioButton.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	radioButton.Show()

	labelDemo4 := ui.GoLabel(layout1, "GoButton :")
	labelDemo4.SetAutoSize(true)
	//labelDemo4.SetSize(140, 30)
	labelDemo4.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo4.Show()

	button = ui.GoButton(layout1, "Exit")
	button.MarginSetLeft(40)
	button.MarginSetTop(0)
	button.SetAutoSize(true)
	button.SetWidth(100)
	button.SetOnClick(ActionExit_Clicked)
	button.SetBorderStyle(ui.BorderSingleLine)
	button.Show()

	r, g, b := ui.ColorToRGB(app.ColorWindow)
	lblText := "(r:" + strconv.Itoa(int(r)) + ", g:" + strconv.Itoa(int(g)) + ", b:" + strconv.Itoa(int(b)) + ")" 
	labelDemo5 := ui.GoLabel(layout1, "ColorWindow " + lblText)
	labelDemo5.SetAutoSize(true)
	//labelDemo4.SetSize(140, 30)
	labelDemo5.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo5.Show()

	panel1 := ui.GoPanel(layout2)
	panel1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	panel1.SetMargin(5,5,5,5)
	
	panel1.SetBorderStyle(ui.BorderRaised)
	panel1.Show()

	panel1layout := panel1.Layout()
	panel1layout.SetStyle(ui.VBoxLayout)
	//panel1layout.SetPadding(10,10,10,10)

	newfont, err := ui.CreateFont("Times", 20, ui.FW_Bold, false)
	if err != nil {log.Println("LogFont Error =", err)}
	edit := ui.GoTextEdit(panel1layout)
	edit.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	edit.SetFont(newfont)
	edit.SetTextColor(ui.Color_Red)
	
	edit.SetBackgroundColor(ui.Color_Orange)
	
	//edit.SetBorderStyle(ui.BorderRaised)
	edit.Show()

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 2, p.Y() + 2, p.Width() - 4, p.Height() - 4)
	
}
func ActionExit_Clicked() {
	ui.GoApp().Exit()	
}

func ActionMessage_Clicked() {
	
}

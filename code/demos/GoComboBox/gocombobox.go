/* win32go/code/demos/GoComboBox/gocombobox.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var combobox1 *ui.GoComboBoxObj


func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoComboBox Demo")
	mainwindow.SetMargin(40, 40, 40, 40)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuComboBox := mainmenu.AddMenu("&ComboBox")
	//+++++++++++++
	menuNewComboBox := menuComboBox.AddMenu("&Show")
	//++++++++++++++++++++
	actionComboBox := menuNewComboBox.AddAction("ComboBox")
	actionComboBox.SetOnActivate(ActionComboBox_Clicked)
	//_____________
	menuMessage := mainmenu.AddMenu("&Message")
	//+++++++++++++
	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	combobox1 = ui.GoComboBox(mainwindow)
	combobox1.SetPosition(300, 200)
	combobox1.SetWidth(150)
	combobox1.SetOnChange(ComboBox1_Changed)
	//combobox1.Show()
	

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 10, p.Y() + 10, p.Width() - 20, p.Height() - 20)
	
}
func ActionExit_Clicked() {
	//messageBox := ui.GoMessageBox(mainwindow, ui.Question, "Application Shutdown.", "Do you really want to exit?")
	//button := messageBox.Exec()
	//log.Println("button =", button)
	//if button == 6 {
		ui.GoApp().Exit()
	//}
	
}

func ActionComboBox_Clicked() {
	options := []string{"First Item", "Second Item", "Third Item"}
	combobox1.SetItems(options)
	//combobox1.SetSelectedIndex(0)

	combobox1.Show()
	combobox1.SetText("Last Item")

}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!\n")
}

func ComboBox1_Changed(id int) {
	log.Println("ComboBox Changed.")
}

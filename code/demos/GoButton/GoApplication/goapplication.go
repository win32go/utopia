/* goapplication.go */

package main

import (
	"os"
	"path/filepath"
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj


func main() {
	appName := filepath.Base(os.Args[0]) 	// GoBasicApplication.exe
	appName = appName[:len(appName) - 4]	// GoBasicApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle(appName + " Demo")
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	menuFile := mainmenu.AddMenu("&File")
	
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)

	app.Run()
}

func ActionExit_Clicked() {
	app.Exit()	
}
/* win32go/code/demos/GoWidgets/gowidgets.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	"strconv"
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var button *ui.GoButtonObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	if filepath.Ext(appName) == ".exe" {
		appName = appName[:len(appName) - 4]	// GoApplication
	}
	log.Println("creating Application..........")
	app = ui.GoApplication(appName)
	//app.SetWindowColor(ui.COLOR_WHITESMOKE)
	log.Println("creating mainwindow..........")
	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle(appName + " Demo")
	mainwindow.SetLayoutSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	mainwindow.SetLayoutStyle(ui.VBoxLayout)
	mainwindow.SetLayoutHeight(800)
	mainwindow.SetLayoutWidth(800)
	//mainwindow.SetLayoutMargin(10, 10, 10, 10)
	mainwindow.SetLayoutPadding(10, 10, 10, 10)
	mainwindow.Layout().SetBorderStyle(ui.BorderSingleLine)
	mainwindow.SetBackgroundColor(ui.Color_Silver)

	//mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	//mainlayout := mainwindow.Layout()
	//hdrlayout := ui.GoLayout(mainlayout, ui.HBoxLayout)
	hdrlayout := mainwindow.AddLayout(ui.HBoxLayout)
	hdrlayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//hdrlayout.SetBackgroundColor(ui.Color_Coral)
	hdrlayout.SetHeight(50)

	//dspllayout := ui.GoLayout(mainlayout, ui.HBoxLayout)
	dspllayout := mainwindow.AddLayout(ui.HBoxLayout)
	dspllayout.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)


	layout1 := dspllayout.AddLayout(ui.VBoxLayout)
	layout1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	//layout1.SetWidth(160)

	layout2 := dspllayout.AddLayout(ui.VBoxLayout)
	layout2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)


	hdrlayout.Show()
	dspllayout.Show()
	layout1.Show()
	layout2.Show()
	
	mainmenu := mainwindow.AddMenuBar()
	  menuFile := mainmenu.AddMenu("&File")
	    /*actionNew :=*/ menuFile.AddAction("&New")
	    /*actionOpen :=*/ menuFile.AddAction("&Open")
	    /*actionSave :=*/ menuFile.AddAction("&Save")
	    /*actionSaveAs :=*/ menuFile.AddAction("Save&As")
	    menuFile.AddSeparator()
	    actionExit := menuFile.AddAction("&Exit")
	    actionExit.SetOnActivate(ActionExit_Clicked)
	  menuMessage := mainmenu.AddMenu("&Message")
	    actionMessage := menuMessage.AddAction("&Message")
	    actionMessage.SetOnActivate(ActionMessage_Clicked)

	hdrpanel := ui.GoPanel(hdrlayout)
	hdrpanel.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	hdrpanel.SetMargin(5,5,5,5)
	//hdrpanel.SetBackgroundMode(ui.TransparentMode)
	hdrpanel.SetBackgroundColor(ui.Color_Silver)
	hdrpanel.Show()   

	hdrlabel := ui.GoLabel(hdrpanel, "Win32Go Widgets Demo ......")
	hdrlabel.SetAutoSize(true)
	//hdrlabel.SetSize(200, 30)
	//hdrlabel.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	hdrlabel.Show()

	labelDemo1 := ui.GoLabel(layout1, "GoLabel :")
	labelDemo1.SetAutoSize(true)
	labelDemo1.MarginSetBottom(0)
	//labelDemo1.SetSize(140, 30)
	//labelDemo1.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo1.Show()

	label := ui.GoLabel(layout1, "Welcome Label")
	label.MarginSetLeft(40)
	label.MarginSetTop(0)
	label.SetAutoSize(true)
	//label.SetSize(200, 30)
	//label.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	label.Show()

	labelDemo2 := ui.GoLabel(layout1, "GoCheckBox :")
	labelDemo2.SetAutoSize(true)
	//labelDemo2.SetSize(140, 30)
	//labelDemo2.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo2.Show()

	checkBox := ui.GoCheckBox(layout1, "Show Desktop Settings")
	checkBox.MarginSetLeft(40)
	checkBox.MarginSetTop(0)
	checkBox.SetAutoSize(true)
	//checkBox.SetSize(200, 30)
	checkBox.SetBorderStyle(ui.BorderSingleLine)
	checkBox.SetBackgroundColor(ui.Color_Silver)
	
	//checkBox.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	checkBox.Show()

	checkBox2 := ui.GoCheckBox(layout1, "Show Desktop Settings")
	checkBox2.MarginSetLeft(40)
	checkBox2.MarginSetTop(0)
	checkBox2.SetAutoSize(true)
	//checkBox.SetSize(200, 30)
	//checkBox2.SetBorderStyle(ui.BorderSingleLine)
	checkBox2.SetBackgroundColor(ui.Color_Silver)

	checkBox2.Show()

	labelDemo3 := ui.GoLabel(layout1, "GoRadioButton :")
	labelDemo3.SetAutoSize(true)
	//labelDemo3.SetSize(140, 30)
	//labelDemo3.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo3.Show()

	radioButton := ui.GoRadioButton(layout1, "Show Desktop Settings")
	radioButton.MarginSetLeft(40)
	radioButton.MarginSetTop(0)
	radioButton.SetAutoSize(true)
	radioButton.SetBorderStyle(ui.BorderRaised)
	radioButton.SetBackgroundColor(ui.Color_Silver)
	//radioButton.Resize()
	//radioButton.SetSize(200, 30)
	//radioButton.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	radioButton.Show()

	labelDemo4 := ui.GoLabel(layout1, "GoButton :")
	labelDemo4.SetAutoSize(true)
	//labelDemo4.SetSize(140, 30)
	//labelDemo4.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo4.Show()

	

	button = ui.GoButton(layout1, "Exit")
	button.MarginSetLeft(40)
	button.MarginSetTop(0)
	button.SetBorderStyle(ui.BorderSingleLine)
	button.SetAutoSize(true)
	//button.SetWidth(100)
	button.SetOnClick(ActionExit_Clicked)
	
	button.Show()

	r, g, b := ui.ColorToRGB(app.ColorWindow)
	lblText := "(r:" + strconv.Itoa(int(r)) + ", g:" + strconv.Itoa(int(g)) + ", b:" + strconv.Itoa(int(b)) + ")" 
	labelDemo5 := ui.GoLabel(layout1, "ColorWindow " + lblText)
	labelDemo5.SetAutoSize(true)
	//labelDemo4.SetSize(140, 30)
	//labelDemo5.SetSizePolicy(ui.ST_FixedWidth, ui.ST_FixedHeight, false)
	labelDemo5.Show()


	panel1 := ui.GoPanel(layout2)
	panel1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	panel1.SetMargin(5,5,5,5)
	panel1.SetBorderStyle(ui.BorderRaised)
	panel1.SetBackgroundColor(ui.Color_Silver)
	panel1.Show()

	panel1layout := panel1.Layout()
	panel1layout.SetStyle(ui.VBoxLayout)

	labelcolors1 := ui.GoLabel(panel1layout, "Windows 10 Colors........")
	labelcolors1.PaddingSetLeft(10)
	labelcolors1.SetBorderStyle(ui.BorderRaised)
	labelcolors1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelcolors1.Show()

	layoutcolors1 := ui.GoLayout(panel1layout, ui.HBoxLayout)
	layoutcolors1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutcolors1.Show()

	layoutindex := ui.GoLayout(layoutcolors1, ui.VBoxLayout)
	layoutindex.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutindex.Show()

	layoutcolor := ui.GoLayout(layoutcolors1, ui.VBoxLayout)
	layoutcolor.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutcolor.Show()

	label1 := ui.GoLabel(layoutindex, "ColorWindow :")
	//label1.SetWidth(240)
	label1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label1.SetAutoSize(true)
	label1.Show()

	label2 := ui.GoLabel(layoutindex, "ColorWindowText :")
	//label2.SetWidth(240)
	label2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label2.SetAutoSize(true)
	label2.Show()

	label3 := ui.GoLabel(layoutindex, "ColorHotLight :")
	//label3.SetWidth(240)
	label3.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label3.SetAutoSize(true)
	label3.Show()

	label4 := ui.GoLabel(layoutindex, "ColorGrayText :")
	//label4.SetWidth(240)
	label4.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label4.SetAutoSize(true)
	label4.Show()

	label5 := ui.GoLabel(layoutindex, "ColorHighlightText :")
	//label5.SetAutoSize(true)
	//label5.SetWidth(240)
	label5.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	label5.Show()

	label6 := ui.GoLabel(layoutindex, "ColorHighlight :")
	//label6.SetWidth(240)
	label6.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label6.SetAutoSize(true)
	label6.Show()

	label7 := ui.GoLabel(layoutindex, "ColorButtonText :")
	//label7.SetWidth(240)
	label7.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label7.SetAutoSize(true)
	label7.Show()

	label8 := ui.GoLabel(layoutindex, "Color3DFace :")
	//label8.SetWidth(240)
	label8.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	//label8.SetAutoSize(true)
	label8.Show()

	color1 := ui.GoLabel(layoutcolor, app.ColorWindow)
	//color1.SetBackgroundColor(app.ColorWindow)
	color1.SetBorderStyle(ui.BorderRaised)
	color1.Show()


	color2 := ui.GoLabel(layoutcolor, app.ColorWindowText)
	//color2.SetBackgroundColor(app.ColorWindowText)
	color2.SetBorderStyle(ui.BorderRaised)
	color2.Show()

	color3 := ui.GoLabel(layoutcolor, app.ColorHotlight)
	//color3.SetBackgroundColor(app.ColorHotlight)
	color3.SetBorderStyle(ui.BorderRaised)
	color3.Show()

	color4 := ui.GoLabel(layoutcolor, app.ColorGrayText)
	//color4.SetBackgroundColor(app.ColorGrayText)
	color4.SetBorderStyle(ui.BorderRaised)
	color4.Show()

	color5 := ui.GoLabel(layoutcolor, app.ColorHighlightText)
	//color5.SetBackgroundColor(app.ColorHighlightText)
	color5.SetBorderStyle(ui.BorderRaised)
	color5.Show()

	color6 := ui.GoLabel(layoutcolor, app.ColorHighlight)
	//color6.SetBackgroundColor(app.ColorHighlight)
	color6.SetBorderStyle(ui.BorderRaised)
	color6.Show()

	color7 := ui.GoLabel(layoutcolor, app.ColorButtonText)
	//color7.SetBackgroundColor(app.ColorButtonText)
	color7.SetBorderStyle(ui.BorderRaised)
	color7.Show()

	color8 := ui.GoLabel(layoutcolor, app.Color3DFace)
	color8.SetBackgroundColor(app.Color3DFace)
	color8.SetBorderStyle(ui.BorderRaised)
	color8.Show()

	labelcolors2 := ui.GoLabel(panel1layout, "Windows XP Colors........")
	labelcolors2.PaddingSetLeft(10)
	labelcolors2.SetBorderStyle(ui.BorderRaised)
	labelcolors2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelcolors2.Show()

	layoutcolors2 := ui.GoLayout(panel1layout, ui.HBoxLayout)
	layoutcolors2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutcolors2.Show()
	
	layoutindexXP := ui.GoLayout(layoutcolors2, ui.VBoxLayout)
	layoutindexXP.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutindexXP.Show()

	layoutcolorXP := ui.GoLayout(layoutcolors2, ui.VBoxLayout)
	layoutcolorXP.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_ExpandingHeight, false)
	layoutcolorXP.Show()

	labelXP1 := ui.GoLabel(layoutindexXP, "ColorActiveBorder :")
	labelXP1.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP1.Show()

	labelXP2 := ui.GoLabel(layoutindexXP, "ColorInactiveBorder :")
	labelXP2.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP2.Show()

	labelXP3 := ui.GoLabel(layoutindexXP, "ColorActiveCaption :")
	labelXP3.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP3.Show()

	labelXP4 := ui.GoLabel(layoutindexXP, "ColorCaptionText :")
	labelXP4.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP4.Show()

	labelXP5 := ui.GoLabel(layoutindexXP, "ColorInactiveCaption :")
	labelXP5.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP5.Show()

	labelXP6 := ui.GoLabel(layoutindexXP, "ColorInactiveCaptionText :")
	labelXP6.SetSizePolicy(ui.ST_ExpandingWidth, ui.ST_FixedHeight, false)
	labelXP6.Show()

	colorXP1 := ui.GoLabel(layoutcolorXP, app.ColorActiveBorder)
	//colorXP1.SetBackgroundColor(app.ColorActiveBorder)
	colorXP1.SetBorderStyle(ui.BorderRaised)
	colorXP1.Show()

	colorXP2 := ui.GoLabel(layoutcolorXP, app.ColorInactiveBorder)
	//colorXP2.SetBackgroundColor(app.ColorInactiveBorder)
	colorXP2.SetBorderStyle(ui.BorderRaised)
	colorXP2.Show()

	colorXP3 := ui.GoLabel(layoutcolorXP, app.ColorActiveCaption)
	//colorXP3.SetBackgroundColor(app.ColorActiveCaption)
	colorXP3.SetBorderStyle(ui.BorderRaised)
	colorXP3.Show()

	colorXP4 := ui.GoLabel(layoutcolorXP, app.ColorCaptionText)
	//colorXP4.SetBackgroundColor(app.ColorCaptionText)
	colorXP4.SetBorderStyle(ui.BorderRaised)
	colorXP4.Show()

	colorXP5 := ui.GoLabel(layoutcolorXP, app.ColorInactiveCaption)
	//colorXP3.SetBackgroundColor(app.ColorActiveCaption)
	colorXP5.SetBorderStyle(ui.BorderRaised)
	colorXP5.Show()

	colorXP6 := ui.GoLabel(layoutcolorXP, app.ColorInactiveCaptionText)
	//colorXP3.SetBackgroundColor(app.ColorActiveCaption)
	colorXP6.SetBorderStyle(ui.BorderRaised)
	colorXP6.Show()

	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() + 2, p.Y() + 2, p.Width() - 4, p.Height() - 4)
	
}
func ActionExit_Clicked() {
	ui.GoApp().Exit()	
}

func ActionMessage_Clicked() {
	
}

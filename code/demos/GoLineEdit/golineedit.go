/* win32go/code/demos/GoLineEdit/golineedit.go */

package main

import (
	"log"
	"os"
	"path/filepath"
	
	ui "gitlab.com/win32go/win32"
)

var app *ui.GoWinApplication
var mainwindow *ui.GoWindowObj
var lineedit1 *ui.GoLineEditObj

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app = ui.GoApplication(appName)

	mainwindow = ui.GoWindow(nil)
	mainwindow.SetTitle("GoMenus Demo")
	mainwindow.SetMargin(40, 40, 40, 40)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	mainmenu := mainwindow.AddMenuBar()
	
	//_____________
	menuFile := mainmenu.AddMenu("&File")
	//+++++++++++++
	actionExit := menuFile.AddAction("&Exit")
	actionExit.SetOnActivate(ActionExit_Clicked)
	//_____________
	menuLineEdit := mainmenu.AddMenu("&LineEdit")
	//+++++++++++++
	actionLineEdit := menuLineEdit.AddAction("&New")
	actionLineEdit.SetOnActivate(ActionLineEdit_Clicked)
	//_____________
	menuMessage := mainmenu.AddMenu("&Message")
	//+++++++++++++
	actionMessage := menuMessage.AddAction("&Message")
	actionMessage.SetOnActivate(ActionMessage_Clicked)

	lineedit1 = ui.GoLineEdit(mainwindow)
	lineedit1.SetPosition(100, 200)
	lineedit1.SetWidth(300)
	
	
	ret := app.Run()
	log.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *ui.GoPainter) {
	blue := ui.ColorFromRGB(0, 0, 255)
	pen := ui.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X() - 10, p.Y() - 10, p.Width() + 20, p.Height() + 20)
	
}
func ActionExit_Clicked() {
	//messageBox := ui.GoMessageBox(mainwindow, ui.Question, "Application Shutdown.", "Do you really want to exit?")
	//button := messageBox.Exec()
	//log.Println("button =", button)
	//if button == 6 {
		ui.GoApp().Exit()
	//}
	
}

func ActionLineEdit_Clicked() {
	lineedit1.Show()
}

func ActionMessage_Clicked() {
	mainwindow.TextOut("Hello World!\n")
}
